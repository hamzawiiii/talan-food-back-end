package com.talan.food.entities;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.talan.food.dto.MenuDayDto;
import com.talan.food.util.JWTUtil;
@SpringBootTest
@AutoConfigureMockMvc
public class ConstractorMenuDayTest {
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void testConstractor() throws Exception{
        String token;
        token = getToken();
        Product prod1 =  new Product(1L,"koseksi","bnine",10,14.5,"image",true,new Category(2L,"pp"), null);
		Product prod2 =  new Product(1L,"koseksi","bnine",10,14.5,"image",true,new Category(2L,"pp"), null);
		List<Product> listProducts = new ArrayList<>();
		listProducts.add(prod1);
		listProducts.add(prod2);
		MenuDayDto menu = new MenuDayDto(null,listProducts);
        this.mockMvc.perform(get("/api/menus")
        		.header("Authorization", "Bearer " + token)
        		.contentType("application/json")
        		.content(asJsonString(menu)))	
        		.andExpect(status().isOk());
	}
	
	@Test
	void testConstractorId() throws Exception{
        String token;
        token = getToken();
        Product prod1 =  new Product(1L,"koseksi","bnine",10,14.5,"image",true,new Category(2L,"pp"), null);
		Product prod2 =  new Product(1L,"koseksi","bnine",10,14.5,"image",true,new Category(2L,"pp"), null);
		List<Product> listProducts = new ArrayList<>();
		listProducts.add(prod1);
		listProducts.add(prod2);
		MenuDayDto menu = new MenuDayDto(1L,null,listProducts);
        this.mockMvc.perform(get("/api/menus")
        		.header("Authorization", "Bearer " + token)
        		.contentType("application/json")
        		.content(asJsonString(menu)))	
        		.andExpect(status().isOk());
	}
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
	
    public String getToken() throws Exception {
        String email = "foulen@gmail.com";
        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET.getBytes());
        List<String> roles = new ArrayList<>();
        roles.add("ADMIN");
        String access_token = JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.EXPIRE_ACCESS_TOKEN))
                .withIssuer("http://localhost:8090/api/users/login")
                .withClaim("roles", roles)
                .sign(algorithm);
        return access_token;
    }
}
