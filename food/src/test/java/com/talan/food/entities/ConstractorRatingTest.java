
package com.talan.food.entities;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.talan.food.util.JWTUtil;

@SpringBootTest
@AutoConfigureMockMvc
public class ConstractorRatingTest {
	  @Autowired
	  private MockMvc mockMvc;
	  
		@Test
		void testConstractor() throws Exception {
			User user = new User();
			Product product = new Product();
			user.setId(4L);
			product.setId(1L);
	        String token;
	        token = getTokenUser();	
		    this.mockMvc.perform(post("/api/products/ratings")
		    		.header("Authorization", "Bearer " + token)
	                .contentType("application/json")
	                .content(asJsonString((new Rating(5.5,user,product)))))
	        		.andExpect(status().isOk());
		}
		@Test
		void testConstractorId() throws Exception {
			User user = new User();
			Product product = new Product();
			user.setId(4L);
			product.setId(1L);
	        String token;
	        token = getTokenUser();	
		    this.mockMvc.perform(post("/api/products/ratings")
		    		.header("Authorization", "Bearer " + token)
	                .contentType("application/json")
	                .content(asJsonString((new Rating(40L,5.5,user,product)))))
	        		.andExpect(status().isOk());
		}
	  
	    public static String asJsonString(final Object obj) {
	        try {
	            return new ObjectMapper().writeValueAsString(obj);
	        } catch (Exception e) {
	            throw new RuntimeException(e);
	        }
	    }    
		
	    public String getToken() throws Exception {
	        String email = "foulen@gmail.com";
	        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET.getBytes());
	        List<String> roles = new ArrayList<>();
	        roles.add("ADMIN");
	        String access_token = JWT.create()
	                .withSubject(email)
	                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.EXPIRE_ACCESS_TOKEN))
	                .withIssuer("http://localhost:8090/api/users/login")
	                .withClaim("roles", roles)
	                .sign(algorithm);
	        return access_token;
	    }
	    
	    public String getTokenUser() throws Exception {
	        String email = "foulen@gmail.com";
	        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET.getBytes());
	        List<String> roles = new ArrayList<>();
	        roles.add("USER");
	        String access_token = JWT.create()
	                .withSubject(email)
	                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.EXPIRE_ACCESS_TOKEN))
	                .withIssuer("http://localhost:8090/api/users/login")
	                .withClaim("roles", roles)
	                .sign(algorithm);
	        return access_token;
	    }
}

