package com.talan.food.servicesImpl;

import ch.qos.logback.core.encoder.ByteArrayUtil;
import com.talan.food.dto.CategoryDto;
import com.talan.food.dto.ProductDto;
import com.talan.food.entities.Category;
import com.talan.food.entities.Product;
import com.talan.food.helpers.ModelMapperConverter;
import com.talan.food.repositories.ProductRepo;
import com.talan.food.services.impl.ProductServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {

	@InjectMocks
	ProductServiceImpl productServiceImpl;
	@Mock
	private ProductRepo productRepo;
	
	@BeforeEach
    void setUp() {
        this.productServiceImpl = new ProductServiceImpl(productRepo);
    }

	@Test
	void testAddProduct( )throws Exception {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		ProductDto prod = new ProductDto(1L, "koseksi","bnine",10,14.5,null,true,new Category(2L,"pp"));
		prod.setImage("https://upload.wikimedia.org/wikipedia/fr/thumb/3/33/Logo_Club_africain.svg/1200px-Logo_Club_africain.svg.png");
		MultipartFile multipartFile = new MockMultipartFile("ca", ByteArrayUtil.hexStringToByteArray("e04fd020ea3a6910a2d808002b30309d"));
		productServiceImpl.saveProduct(prod, multipartFile);
        Product p = ModelMapperConverter.map(prod, Product.class);
//        then
        ArgumentCaptor<Product> argumentCaptor = ArgumentCaptor.forClass(Product.class);
        verify(productRepo).save(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().toString()).hasToString(p.toString());
	}

	@Test
	void testAddProductWhenImageNull( )throws Exception {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		ProductDto prod = new ProductDto(1L, "koseksi","bnine",10,14.5,null,true,new Category(2L,"pp"));
		prod.setImage("https://upload.wikimedia.org/wikipedia/fr/thumb/3/33/Logo_Club_africain.svg/1200px-Logo_Club_africain.svg.png");
		when(productRepo.getById(1L)).thenReturn(ModelMapperConverter.map(prod, Product.class));
		productServiceImpl.saveProduct(prod, null);
//        then
        verify(productRepo).save(ModelMapperConverter.map(prod, Product.class));
	}

	@Test
	void testAddProductAndCreateDirectory( )throws Exception {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		ProductDto prod = new ProductDto(1L, null,"bnine",10,14.5,null,true,new Category(2L,"pp"));
		when(productRepo.getById(1L)).thenReturn(ModelMapperConverter.map(prod, Product.class));
		productServiceImpl.saveProduct(prod, null);
//        then
        verify(productRepo).save(ModelMapperConverter.map(prod, Product.class));
	}

	
	@Test
	void testProfileImage( )throws Exception {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		Product prod = new Product(1L, "koseksi","bnine",10,14.5,null,true,new Category(2L,"pp"), null);
		prod.setImage("https://upload.wikimedia.org/wikipedia/fr/thumb/3/33/Logo_Club_africain.svg/1200px-Logo_Club_africain.svg.png");
		MultipartFile multipartFile = new MockMultipartFile("ca", ByteArrayUtil.hexStringToByteArray("e04fd020ea3a6910a2d808002b30309d"));
		productServiceImpl.saveProfileImage(prod, multipartFile);
//        then
        ArgumentCaptor<Product> argumentCaptor = ArgumentCaptor.forClass(Product.class);
        verify(productRepo).save(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().toString()).hasToString(prod.toString());
	}
	
	@Test
	void testGetProductById() throws Exception {
		productServiceImpl.getProductById(1L);
		verify(productRepo).findById(1L);
	}

	@Test
	void testGetAllProducts() throws Exception {
		productServiceImpl.getAllProducts();
		verify(productRepo).findAllByOrderById();
	}

	@Test
	void testGetByCategoryId() throws Exception {
		productServiceImpl.getByCategoryId(2L);
		verify(productRepo).findByCategoryIdOrderByIdAsc(2L);
	}
	
	@Test
	void testGetProductsByCategory() throws Exception {
		CategoryDto cat = new CategoryDto(2L,"pp");
		productServiceImpl.getProductsByCategory(cat);
		verify(productRepo).findByCategoryOrderByIdAsc(ModelMapperConverter.map(cat, Category.class));
	}
	 
	@Test
	void testUpdateProductQuantity() throws Exception {
		ProductDto prod = new ProductDto(1L,"koseksi","bnine",10,14.5,"image",true,new Category(2L,"pp"));
		productServiceImpl.updateProduct(prod);
		verify(productRepo).save(ModelMapperConverter.map(prod, Product.class));
	}

	@Test
    void canDeleteById() {
        //        given
        Long id = 9L;
//        when
        productServiceImpl.deleteProductById(id);
//        then
		verify(productRepo).deleteById(id);
		assertThat(productRepo.findById(id)).isEmpty();
    }
}


