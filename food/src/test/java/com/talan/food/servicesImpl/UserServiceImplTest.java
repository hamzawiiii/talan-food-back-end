package com.talan.food.servicesImpl;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.talan.food.dto.UserDto;
import com.talan.food.entities.Role;
import com.talan.food.entities.User;
import com.talan.food.helpers.ModelMapperConverter;
import com.talan.food.repositories.RoleRepo;
import com.talan.food.repositories.UserRepo;
import com.talan.food.services.impl.UserServiceImpl;


@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {
    @Mock
    private UserRepo userRepo;
    @Mock
    private RoleRepo roleRepo;
    private UserServiceImpl userService;
    @Mock
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    void setUp() {
        this.userService = new UserServiceImpl(this.userRepo, this.roleRepo, this.passwordEncoder);
    }

    @Test
    void canSignup() {
//        given
        Role role = new Role();
        role.setId(2L);
        UserDto userDto = new UserDto("Foulen", "Ben Falten", "foulen@gmail.com", "foulenbenfalten", "21001920", role);
//        when
        userService.signup(userDto);
        User user = ModelMapperConverter.map(userDto, User.class);
//        then
        ArgumentCaptor<User> argumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepo).save(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().toString()).hasToString(user.toString());
    }

    @Test
    void canGetUserById() {
        //        given
        Long id = 7L;
//        when
        userService.getUserById(id);
//        then
        verify(userRepo).findById(id);
    }

    @Test
    void canLoadUserByUsername() {
        UserDto userDto = new UserDto("Foulen", "Ben Falten", "ca@gmail.com", "foulenbenfalten", "21001920", new Role(1L, "user"));
        when(userRepo.findByEmail("ca@gmail.com")).thenReturn(ModelMapperConverter.map(userDto, User.class));
        assertThat(userService.loadUserByUsername("ca@gmail.com").getUsername()).isEqualTo("ca@gmail.com");
    }

    @Test
    void canLoadUserByUsernameThrowException() {
        when(userRepo.findByEmail("ca@gmail.com")).thenReturn(null);
        assertThatThrownBy(() -> userService.loadUserByUsername("ca@gmail.com")).isInstanceOf(UsernameNotFoundException.class);
        ;
    }

    @Test
    void canGetUserByEmail() {
        //        given
        String email = "foulen@gmail.com";
//        when
        when(userService.getUserByEmail(email)).thenThrow(new NullPointerException());
        userService.getUserByEmail(email);
//        then
        verify(userRepo).findByEmail(email);
    }
    
}
