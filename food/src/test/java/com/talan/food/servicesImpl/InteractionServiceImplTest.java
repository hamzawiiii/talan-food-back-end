package com.talan.food.servicesImpl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import com.talan.food.dto.InteractionDto;
import com.talan.food.entities.Interaction;
import com.talan.food.entities.Role;
import com.talan.food.entities.User;
import com.talan.food.repositories.InteractionRepo;
import com.talan.food.repositories.UserRepo;
import com.talan.food.services.impl.InteractionServiceImpl;

@ExtendWith(MockitoExtension.class)
class InteractionServiceImplTest {

    @InjectMocks
    InteractionServiceImpl InteractionServiceImpl;
    @Mock
    InteractionRepo interactionRepo;
    @Mock
    UserRepo userRepo;
    @Mock
    private JavaMailSender javaMailSender;

    @Test
    void testGetAllInteraction() throws Exception {
        InteractionServiceImpl.getAllInteraction();
        verify(interactionRepo).findAllByOrderById();
    }

    @Test
    void testGetInteractionById() throws Exception {
        InteractionServiceImpl.getInteractionById(2L);
        verify(interactionRepo).findById(2L);
    }

    @Test
    void testGetInteractionByReclamation() throws Exception {
        InteractionServiceImpl.getInteractionByReclamation();
        verify(interactionRepo).findAllByTypeOrderByIdDesc("Reclamation");
    }

    @Test
    void testGetInteractionBySuggestion() throws Exception {
        InteractionServiceImpl.getInteractionBySuggestion();
        verify(interactionRepo).findAllByTypeOrderByIdDesc("Suggestion");
    }

    @Test
    void testGetInteractionByNote() throws Exception {
        InteractionServiceImpl.getInteractionByNote();
        verify(interactionRepo).findAllByTypeOrderByIdDesc("Note");
    }

    @Test
    void testAddInteraction() throws Exception {
        User user = new User();
        when(userRepo.findById(anyLong())).thenReturn(Optional.of(user));
        InteractionDto interactionDto = new InteractionDto("Reclamation", "quantité reduite", 2, new User(4L, "ali", "bouzaien", "ali.bouzaiene@talan.com", "00000000", "53250795", new Role(2L, "USER"), false));

        assertEquals("Reclamation", InteractionServiceImpl.addInteraction(interactionDto).getType());
    }

    @Test
    void testAnswerReclamationsClient() throws Exception {
        Interaction interaction = new Interaction(7L,"Reclamation", "quantité reduite", 2, new User(4L, "ali", "bouzaien", "ali.bouzaiene@talan.com", "00000000", "53250795", new Role(2L, "USER"), false));

        when(interactionRepo.getById(anyLong())).thenReturn(interaction);
        InteractionServiceImpl.answerReclamationsClient(7L, "ca");
        verify(javaMailSender).send((SimpleMailMessage) any());
    }

    @Test
    void testAnswerToSuggession() throws Exception {

        Interaction interaction = new Interaction(7L,"Reclamation", "quantité reduite", 2, new User(4L, "ali", "bouzaien", "ali.bouzaiene@talan.com", "00000000", "53250795", new Role(2L, "USER"), false));

        when(interactionRepo.getById(anyLong())).thenReturn(interaction);
        InteractionServiceImpl.answerToSuggession(7L);
        verify(javaMailSender).send((SimpleMailMessage) any());
    }

    @Test
    void canDeleteById() {
        //        given
        Long id = 186L;
//        when
        InteractionServiceImpl.deleteInteractionById(id);
//        then
        assertThat(interactionRepo.findById(id)).isEmpty();
    }
}

