package com.talan.food.servicesImpl;

import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.talan.food.repositories.CategoryRepo;
import com.talan.food.services.impl.CategoryServiceImpl;

@SpringBootTest

class CategoryServiceImplTest {

	@Mock
	CategoryRepo categoryRepo;
	@InjectMocks
	CategoryServiceImpl categoryServiceImpl;
	
	@Test
	void getCategorieTest() throws Exception {
		categoryServiceImpl.getAllCategories();
		verify(categoryRepo).findAll();
	}
	
	@Test
	void testGetCategorieById() throws Exception {
		categoryServiceImpl.getCategorieById(1L);
		verify(categoryRepo).findById(1L);
	}
	
	

}
