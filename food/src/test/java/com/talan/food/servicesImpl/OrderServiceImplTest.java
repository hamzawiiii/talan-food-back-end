package com.talan.food.servicesImpl;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.talan.food.dto.OrderDto;
import com.talan.food.entities.Category;
import com.talan.food.entities.Order;
import com.talan.food.entities.Product;
import com.talan.food.entities.Reservation;
import com.talan.food.entities.Role;
import com.talan.food.entities.User;
import com.talan.food.helpers.ModelMapperConverter;
import com.talan.food.repositories.OrderRepo;
import com.talan.food.repositories.ProductRepo;
import com.talan.food.repositories.ReservationRepo;
import com.talan.food.services.impl.OrderServiceImpl;



@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest {
	
	@InjectMocks
	private OrderServiceImpl orderServiceImpl;
	@Mock
	private OrderRepo orderRepo;
	@Mock
	private ProductRepo productRepo;
	@Mock
	private ReservationRepo rerservationRepo;
	
	@Test
	void testGetAllOrders() throws Exception {
		orderServiceImpl.getAllOrders();
		verify(orderRepo).findAll();
	}

	@Test
	void testGetByResrvationId() throws Exception {
		orderServiceImpl.getByResrvationId(7L);
		verify(orderRepo).findOrderByReservationId(7L);
	}
	@Test
	void testFindOrderById() throws Exception{
		orderServiceImpl.findOrderById(1L);
		verify(orderRepo).findById(1L);
	}
	@Test
	void testAddOrder() throws Exception{
		Product product = new Product(1L,"koseksi","bnine",10,14.5,"image",true,new Category(2L,"pp"), null);
		Reservation reservation = new Reservation(1L,15.5,new User(5L,"oussama", "ouss","oussama@talan.com","00000000", "12345678", new Role(2L,"USER"), false),LocalDate.now(), false, false);
		OrderDto orderDto = new OrderDto(3L,product,5,reservation);
		when(productRepo.findById(anyLong())).thenReturn(Optional.of(product));
		when(rerservationRepo.findById(anyLong())).thenReturn(Optional.of(reservation));
		orderServiceImpl.addOrder(orderDto);
		verify(orderRepo).save(ModelMapperConverter.map(orderDto, Order.class));
	}
	
	@Test
    void canDeleteById() {
        //        given
        Long id = 117L;
//        when
        orderServiceImpl.deleteOrder(id);
//        then
        assertThat(orderRepo.findById(id)).isEmpty();
    }
}
