package com.talan.food.servicesImpl;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.talan.food.dto.RatingDto;
import com.talan.food.entities.Product;
import com.talan.food.entities.Rating;
import com.talan.food.entities.User;
import com.talan.food.repositories.ProductRepo;
import com.talan.food.repositories.RatingRepo;
import com.talan.food.repositories.UserRepo;
import com.talan.food.services.impl.RatingServiceImpl;

@ExtendWith(MockitoExtension.class)
class RatingServiceImplTest {

    @Mock
    RatingRepo ratingRepo;
    @Mock
    ProductRepo productRepo;
    @Mock
    UserRepo userRepo;
    @InjectMocks
    RatingServiceImpl ratingServiceImpl;

    @Test
    void getRatingTest() throws Exception {
        Product product = new Product();
        when(productRepo.findById(any())).thenReturn(Optional.of(product));
        Rating rating = new Rating();
        List<Rating> ratings = new ArrayList<>();
        ratings.add(rating);
        when(ratingRepo.findByProduct(product)).thenReturn(ratings);
        ratingServiceImpl.getRating(1L);
        verify(ratingRepo).findByProduct(product);
    }

    @Test
    void addRatingTest() throws Exception {
       User user = new User();
        Product product = new Product();
      user.setId(1L);
       product.setId(2L);
      RatingDto ratingDto = new RatingDto(1, user, product);
        when(userRepo.findById(any())).thenReturn(Optional.of(user));
        when(productRepo.findById(any())).thenReturn(Optional.of(product));
		ratingServiceImpl.addRating(ratingDto);
		
    }

}
