package com.talan.food.servicesImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.talan.food.dto.SubscriptionDto;
import com.talan.food.entities.Product;
import com.talan.food.entities.Subscription;
import com.talan.food.entities.User;
import com.talan.food.helpers.ModelMapperConverter;
import com.talan.food.repositories.SubscriptionRepo;
import com.talan.food.repositories.UserRepo;
import com.talan.food.services.impl.SubscriptionServiceImpl;

@ExtendWith(MockitoExtension.class)
 class SubscriptionServiceImplTest {

	@Mock
	UserRepo userRepo;

	@Mock
	SubscriptionRepo subscriptionRepository;

	@InjectMocks
	SubscriptionServiceImpl subscriptionService;

	@Test
	void testChangeSubscriptionStatus() {

		Subscription sub = new Subscription();
		SubscriptionDto subDto = new SubscriptionDto();
		List<Subscription> subs = new ArrayList<>();
		Product product = new Product();
		User user = new User();
		product.setId(1L);
		user.setId(2L);
		sub.setId(1L);
		sub.setProduct(product);
		sub.setUser(user);
		sub.setActivated(true);
		subDto.setProduct(product);
		subDto.setUser(user);
		subs.add(sub);
		when(subscriptionRepository.findByProductIdAndUserId(1L, 2L)).thenReturn(subs);
		when(subscriptionRepository.saveAll(subs)).thenReturn(subs);
		SubscriptionDto dto = subscriptionService.changeSubscriptionStatus(subDto);
		assertEquals(dto, ModelMapperConverter.map(sub, SubscriptionDto.class));
	}

	@Test
	void findByProductId() {

		Subscription sub = new Subscription();
		SubscriptionDto subDto = new SubscriptionDto();
		List<Subscription> subs = new ArrayList<>();
		Product product = new Product();
		User user = new User();
		product.setId(1L);
		user.setId(2L);
		sub.setId(3L);
		sub.setProduct(product);
		sub.setUser(user);
		sub.setActivated(true);
		subDto.setProduct(product);
		subDto.setUser(user);
		subs.add(sub);
		when(subscriptionRepository.findByProductIdAndActivated(3L, true)).thenReturn(subs);

		List<SubscriptionDto> subsdto = subscriptionService.findByProductId(3L);
		assertEquals(subsdto, ModelMapperConverter.mapAll(subs, SubscriptionDto.class));
	}

	@Test
	void countNotSeenNotifications() {

		when(subscriptionRepository.countByMenuDateNotNullAndUserIdAndSeen(3L, false)).thenReturn(4);

		int number = subscriptionService.countNotSeenNotifications(3L, false);
		assertEquals(4, number);
	}

	@Test
	void getSubscriptions() {
		Subscription sub = new Subscription();
		User user = new User();
		user.setId(2L);
		sub.setUser(user);

		List<Subscription> subs = new ArrayList<>();
		subs.add(sub);
		when(subscriptionRepository.findByMenuDateNotNullAndUserIdOrderByIdDesc(2L)).thenReturn(subs);

		List<SubscriptionDto> subsDto = subscriptionService.getSubscriptions(2L);
		assertEquals(subsDto, ModelMapperConverter.mapAll(subs, SubscriptionDto.class));
	}

	@Test
	void changeToSeen() {
		Subscription sub = new Subscription();
		sub.setId(1L);
		User user = new User();
		user.setId(2L);
		sub.setUser(user);
		when(subscriptionRepository.getById(1L)).thenReturn(sub);
		when(subscriptionRepository.save(sub)).thenReturn(sub);
		SubscriptionDto subDto = subscriptionService.changeToSeen(1L);
		assertEquals(true,subDto.isSeen());

	}

	@Test
	void getSubscriptionStatus() {
		Subscription sub = new Subscription();
		sub.setId(1L);
		User user = new User();
		Product product = new Product();
		product.setId(1L);
		user.setId(2L);
		sub.setUser(user);
		sub.setProduct(product);
		sub.setActivated(true);

		when(subscriptionRepository.findFirst1ByProductIdAndUserId(1L, 2L)).thenReturn(sub);
		
		boolean subactivatedStatus = subscriptionService.getSubscriptionStatus(1L,2L);
		assertEquals(true,subactivatedStatus);

	}
	
	@Test
	void getSubscriptionStatusNull() {
		

		when(subscriptionRepository.findFirst1ByProductIdAndUserId(1L, 2L)).thenReturn(null);
		
		boolean subactivatedStatus = subscriptionService.getSubscriptionStatus(1L,2L);
		assertEquals( false,subactivatedStatus);

	}


}
