package com.talan.food.servicesImpl;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;

import com.talan.food.dto.ReservationDto;
import com.talan.food.entities.Order;
import com.talan.food.entities.Product;
import com.talan.food.entities.Reservation;
import com.talan.food.entities.Role;
import com.talan.food.entities.User;
import com.talan.food.repositories.OrderRepo;
import com.talan.food.repositories.ProductRepo;
import com.talan.food.repositories.ReservationRepo;
import com.talan.food.repositories.UserRepo;
import com.talan.food.services.impl.ReservationServiceImpl;

@ExtendWith(MockitoExtension.class)
class ReservationServiceImplTest {

	@Mock
	ReservationRepo reservationRepo;
	@Mock
	UserRepo userRepo;
	@Mock
	OrderRepo orderrepo;
	@Mock
	ProductRepo productRepo;
	@Mock
	private JavaMailSender javaMailSender;

	@InjectMocks
    ReservationServiceImpl reservationServiceImpl;

    
	@Test
    void testService() throws Exception {
    	reservationServiceImpl.getReservationById(1L);
    	verify(reservationRepo).findById(1L);
    }
	

	@Test
    void testGetAllReservation() {
		reservationServiceImpl.getAllReservation();
		verify(reservationRepo).findAllByOrderById();
    }

	
	@Test
    void testGetReservationByDate() throws Exception {
		LocalDate date = LocalDate.now();
		reservationServiceImpl.getReservationByDate(date);
		verify(reservationRepo).findAllByDateOrderById(date);
    }
	
	

	
	
	/*
	 * @Test void testaddReservation() throws Exception{ User user= new
	 * User(3L,"ali", "bouzaiene","ali.bouzaiene@talan.com","09713359", "53250795",
	 * new Role(2L,"USER"), false); ReservationDto reservationDto = new
	 * ReservationDto(20.5, user, LocalDate.now()); assertEquals(20.5,
	 * reservationServiceImpl.addReservation(reservationDto).getPrice()); }
	 */
	
	
	@Test 
	void testEditReservation() throws Exception{

		User user= new User(3L,"ali", "bouzaiene","ali.bouzaiene@talan.com","09713359", "53250795", new Role(2L,"USER"), false);
		Optional<User> userDto= Optional.of(new User(3L, "ali", "bouzaiene", "ali.bouzaiene@talan.com", "09713359", "53250795", new Role(2L, "USER"), false));
		when(userRepo.findById(3L)).thenReturn(userDto);
		ReservationDto reservationDto = new ReservationDto(19L, 14.5, user,LocalDate.now(), false, false);

		assertEquals(14.5, reservationServiceImpl.editReservation(reservationDto).getPrice());	
	}
	
	@Test 
	void testConfirmReservation() throws Exception{

		User user= new User(3L,"ali", "bouzaiene","ali.bouzaiene@talan.com","09713359", "53250795", new Role(2L,"USER"), false);
		Optional<User> userDto= Optional.of(new User(3L, "ali", "bouzaiene", "ali.bouzaiene@talan.com", "09713359", "53250795", new Role(2L, "USER"), false));
		when(userRepo.findById(3L)).thenReturn(userDto);
		ReservationDto reservationDto = new ReservationDto(19L, 14.5, user,LocalDate.now(), false, false);

		assertEquals(14.5, reservationServiceImpl.confirmReservation(reservationDto).getPrice());	
	}	
	@Test
	void testAnnulerReservationAndReturnTrue()throws Exception{

		Reservation reservation = new Reservation(7L, 14.5, new User(), LocalDate.ofYearDay(5000, 17), true, false);

		when(reservationRepo.getById(7L)).thenReturn(reservation);
		List<Order> orders = new ArrayList<>();
		Product product = new Product();
		product.setId(17L);
		orders.add(new Order(1L, product, 7, reservation));
		when(orderrepo.findOrderByReservationId(7L)).thenReturn(orders);
		when(productRepo.getById(17L)).thenReturn(new Product());
		assertEquals(true, reservationServiceImpl.annulerReservation(7L));
	}

	@Test
	void testAnnulerReservationAndReturnFalse()throws Exception{
		when(reservationRepo.getById(7L)).thenReturn(new Reservation());
		assertEquals(false, reservationServiceImpl.annulerReservation(7L));

	}

	/*
	 * @Test void testAnnulerReservationAndReturnFalse()throws Exception{
	 * when(reservationRepo.getById(7L)).thenReturn(new Reservation());
	 * assertEquals(false, reservationServiceImpl.annulerReservation(7L)); }
	 */
	
	@Test
    void canDeleteById() {
        //        given
        Long id = 149L;
//        when
        reservationServiceImpl.deleteReservationById(id);
//        then
        assertThat(reservationRepo.findById(id)).isEmpty();
    }
	
	@Test
    void testGetAllReservationByDate() {
		reservationServiceImpl.getcountReservation();
		verify(reservationRepo).getcountReservationByDate();
    }
	@Test
	 void testGetAllReservationPrice() {
		 reservationServiceImpl.getIncomesByDate();
		 verify(reservationRepo).getPriceReservationByDate();
	 }
	@Test
	 void testgetIncomesByUser() {
		 reservationServiceImpl.getIncomesByDate();
		 verify(reservationRepo).getPriceReservationByDate();
	 }
	@Test
	 void TestgetIncomesByUser() {
		reservationServiceImpl.getIncomesByUser() ;
		 verify(reservationRepo).getPriceReservationByUser();
	}
	@Test
	
		 void TestgetIncomesByUserMonth() {
			 reservationServiceImpl.getIncomesByMonth() ;
			 verify(reservationRepo).getIncomeByMonth();}
	
	
	@Test
	
	 void TestgetIncomesByYear() {
		 reservationServiceImpl.getIncomesByYear() ;
		 verify(reservationRepo).getIncomeByYear();}



	@Test
	
	 void TestgetReservationByYear() {
		reservationServiceImpl.getReservationByYear();
		 verify(reservationRepo).getReservationByYear();
	}
		}


