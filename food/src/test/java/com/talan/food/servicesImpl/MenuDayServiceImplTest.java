package com.talan.food.servicesImpl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.talan.food.dto.MenuDayDto;
import com.talan.food.dto.ProductDto;
import com.talan.food.entities.Category;
import com.talan.food.entities.MenuDay;
import com.talan.food.entities.Product;
import com.talan.food.helpers.ModelMapperConverter;
import com.talan.food.repositories.MenuRepo;
import com.talan.food.services.ProductService;
import com.talan.food.services.impl.MenuDayServiceImpl;

@ExtendWith(MockitoExtension.class)
class MenuDayServiceImplTest {
	
	@InjectMocks
	MenuDayServiceImpl menuDayServiceImpl;
	
	@Mock
	ProductService productService;
	@Mock
	private MenuRepo menuRepo;

	
	@Test 
	void testSaveMenuDay() throws Exception{		
		
		
		LocalDate date = LocalDate.now();	
		
	     Product prod1 =  new Product(1L,"koseksi","bnine",10,14.5,"image",true,new Category(2L,"pp"), null);
		Product prod2 =  new Product(1L,"koseksi","bnine",10,14.5,"image",true,new Category(2L,"pp"), null);
		List<Product> listProducts = new ArrayList<>();
		listProducts.add(prod1);
		listProducts.add(prod2);
		MenuDay menu = new MenuDay(date,listProducts);
		List<MenuDay> menuDayDtoList = new ArrayList<>();
		menuDayDtoList.add(menu);
		when(menuRepo.findAllByOrderByDate()).thenReturn(menuDayDtoList);
		menuDayServiceImpl.saveMenuDay(ModelMapperConverter.map(menu, MenuDayDto.class));
		verify(menuRepo).save(menu);
	}
	

	@Test 
	void testGetMenuDayById() throws Exception{
		menuDayServiceImpl.getMenuDayById(36L);
		verify(menuRepo).findById(36L);
	}
	
	@Test 
	void testGetMenuDayByDate() throws Exception{
		LocalDate date = LocalDate.now();
		menuDayServiceImpl.getMenuDayByDate(date);
		verify(menuRepo).findByDate(date);
	}
	
	@Test 
	void testGetMenuProducts() throws Exception{
		LocalDate date = LocalDate.now();
		List<Product> listProducts = new ArrayList<>();
		MenuDay menu = new MenuDay(date,listProducts);
		when(menuRepo.findById(40L)).thenReturn(Optional.of(menu));
		menuDayServiceImpl.getMenuProducts(40L);
		verify(menuRepo).findById(40L);
	}
	
	@Test
	void testAddProductToMenuDay() throws Exception{
		ProductDto prod1 =  new ProductDto(1L,"koseksi","bnin",14,17,"true",true, new Category(2L,"pp"));
		when(productService.getProductById(2L)).thenReturn(prod1);

		LocalDate date = LocalDate.now();
		List<Product> listProducts = new ArrayList<>();
		MenuDay menu = new MenuDay(date,listProducts);
		when(menuRepo.getById(35L)).thenReturn(menu);
		menuDayServiceImpl.addProductToMenuDay(35L, 2L);
		verify(menuRepo, times(2)).save(menu);
	}
	
	@Test
    void canDeleteById() {
        //        given
        Long id = 143L;
//        when
        menuDayServiceImpl.deleteMenuDayById(id);
//        then
        assertThat(menuRepo.findById(id)).isEmpty();
    }
    
}
