package com.talan.food.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.talan.food.dto.CategoryDto;
import com.talan.food.dto.ProductDto;
import com.talan.food.entities.Category;
import com.talan.food.services.CategoryService;
import com.talan.food.services.ProductService;
import com.talan.food.services.UserService;
import com.talan.food.util.JWTUtil;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

    @MockBean
    ProductService productService;
    @MockBean
    CategoryService categoryService;
    @MockBean
    UserDetailsService userDetailsService;
    @MockBean
    PasswordEncoder passwordEncoder;
    @MockBean
    UserService userService;
    @Autowired
    private MockMvc mockMvc;

    MockMultipartFile profileImage = new MockMultipartFile("profileImage", "filename.txt", "text/plain", "some xml".getBytes());

    @Test
    void testAddProduct() throws Exception {
        String token;
        token = getToken();
        ProductDto productDto = new ProductDto();
        productDto.setId(7L);
        when(productService.saveProduct(productDto, profileImage)).thenReturn(productDto);
        when(categoryService.getCategorieById(7L)).thenReturn(new CategoryDto());
        this.mockMvc.perform(MockMvcRequestBuilders.multipart("/api/products/new/product")
                        .file(profileImage)
                        .param("name", "makrouna")
                        .param("description", "bel allouch")
                        .param("price", String.valueOf(10.5))
                        .param("quantity", String.valueOf(3))
                        .param("displayed", String.valueOf(true))
                        .param("category", String.valueOf(7L))
                        .header("Authorization", "Bearer " + token)
                )
                .andExpect(status().isOk());
    }


    @Test
    void testSaveProfileImage() throws Exception {
        String token;
        token = getToken();
        ProductDto productDto = new ProductDto();
        productDto.setId(7L);
        when(productService.saveProduct(productDto, profileImage)).thenReturn(productDto);
        when(categoryService.getCategorieById(7L)).thenReturn(new CategoryDto());
        this.mockMvc.perform(MockMvcRequestBuilders.multipart("/api/products/new/product")
                        .file(profileImage)
                        .param("id", "7")
                        .param("name", "makrouna")
                        .param("description", "bel allouch")
                        .param("price", String.valueOf(10.5))
                        .param("quantity", String.valueOf(3))
                        .param("displayed", String.valueOf(true))
                        .param("category", String.valueOf(7L))
                        .header("Authorization", "Bearer " + token)
                )
                .andExpect(status().isOk());
    }


    @Test
    void testGetProducts() throws Exception {
        String token;
        token = getToken();
        ProductDto productDto = new ProductDto();
        productDto.setId(7L);
        List<ProductDto> productDtos = new ArrayList<>();
        productDtos.add(productDto);
        when(productService.getAllProducts()).thenReturn(productDtos);
        this.mockMvc.perform(get("/api/products")
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(7)));
    }

    @Test
    void testGetProductById() throws Exception {
        String token;
        token = getToken();
        ProductDto productDto = new ProductDto();
        productDto.setId(7L);
        when(productService.getProductById(2L)).thenReturn(productDto);
        this.mockMvc.perform(get("/api/products/product/{id}", 2)
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(7)));
    }

    @Test
    void testGetProductsByCategory() throws Exception {
        String token;
        token = getToken();
        Category cat = new Category(1L, "entree");
        this.mockMvc.perform(get("/api/products/category")
                .header("Authorization", "Bearer " + token)
                .contentType("application/json")
                .content(asJsonString(cat))).andExpect(status().isOk());
    }


    @Test
    void testGetProductsByCategoryId() throws Exception {
        String token;
        token = getToken();
        ProductDto productDto = new ProductDto();
        productDto.setId(7L);
        List<ProductDto> productDtos = new ArrayList<>();
        productDtos.add(productDto);
        when(productService.getByCategoryId(2L)).thenReturn(productDtos);
        this.mockMvc.perform(get("/api/products/catid/{catId}", 2)
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(7)));
    }

    @Test
    void testUpdateProductQuantity() throws Exception {
        String token;
        token = getTokenUser();
        ProductDto productDto = new ProductDto();
        ProductDto productDtoAfterUpdate = new ProductDto();
        productDto.setId(7L);
        productDtoAfterUpdate.setId(7L);
        productDtoAfterUpdate.setQuantity(27);
        when(productService.getProductById(7L)).thenReturn(productDto);
        when(productService.updateProduct(productDto)).thenReturn(productDtoAfterUpdate);
        this.mockMvc.perform(post("/api/products/quantity/{id}/{q}", 7, 27)
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("quantity", is(27)));

    }

    @Test
    void testUpdateProfileImage() throws Exception {
        String token;
        token = getToken();
        ProductDto productDto = new ProductDto();
        productDto.setId(7L);
        when(productService.getProductById(7L)).thenReturn(productDto);
        this.mockMvc.perform(MockMvcRequestBuilders.multipart("/api/products/image")
                        .file(profileImage)
                        .param("product", "7")
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk());
    }

    @Test
    void testGetProfileImage() throws Exception {
       // this.mockMvc.perform(get("/api/products/images/{productName}/{fileName}", "aaa", "aaa"))
              //  .andExpect(status().isOk());
    }

    @Test
    void testDeleteProduct() throws Exception {
        String token;
        token = getToken();
        this.mockMvc.perform(delete("/api/products/{id}", 4)
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk());

    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String getToken() throws Exception {
        String email = "foulen@gmail.com";
        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET.getBytes());
        List<String> roles = new ArrayList<>();
        roles.add("ADMIN");
        String access_token = JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.EXPIRE_ACCESS_TOKEN))
                .withIssuer("http://localhost:8090/api/users/login")
                .withClaim("roles", roles)
                .sign(algorithm);
        return access_token;
    }

    public String getTokenUser() throws Exception {
        String email = "foulen@gmail.com";
        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET.getBytes());
        List<String> roles = new ArrayList<>();
        roles.add("USER");
        String access_token = JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.EXPIRE_ACCESS_TOKEN))
                .withIssuer("http://localhost:8090/api/users/login")
                .withClaim("roles", roles)
                .sign(algorithm);
        return access_token;
    }

}
