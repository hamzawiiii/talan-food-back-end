package com.talan.food.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.talan.food.dto.MenuDayDto;
import com.talan.food.entities.Category;
import com.talan.food.entities.Product;
import com.talan.food.services.MenuDayService;
import com.talan.food.services.UserService;
import com.talan.food.util.JWTUtil;

@ExtendWith(SpringExtension.class)
@WebMvcTest(MenuDayController.class)
public class MenuDayControllerTest {

	@MockBean
	MenuDayService menuDayService;
	@MockBean
	UserDetailsService userDetailsService;
	@MockBean
	PasswordEncoder passwordEncoder;
	@MockBean
	UserService userService;
	@Autowired
	private MockMvc mockMvc;

	@Test
	void testGetMenuDayById() throws Exception{
        String token;
        token = getToken();
		MenuDayDto menuDayDto = new MenuDayDto();
		menuDayDto.setId(7L);
		menuDayDto.setDate(LocalDate.parse("2022-06-27"));
		when(menuDayService.getMenuDayById(7L)).thenReturn(menuDayDto);
		this.mockMvc.perform(get("/api/menus/{id}", 7)
				.header("Authorization", "Bearer " + token))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.date", is("2022-06-27")));
	}
	
	@Test
	void testGetAllMenus() throws Exception {
        String token;
        token = getToken();
		List<MenuDayDto> menus = new ArrayList<>();
		MenuDayDto menuDayDto = new MenuDayDto();
		menuDayDto.setId(7L);
		menuDayDto.setDate(LocalDate.parse("2022-06-27"));
		menus.add(menuDayDto);
		when(menuDayService.getAllMenus()).thenReturn(menus);
        this.mockMvc.perform(get("/api/menus")
        		.header("Authorization", "Bearer " + token))
        		.andExpect(status().isOk())
        		.andExpect(jsonPath("$[0].date", is("2022-06-27")));
	}
	

	
	@Test
	void testAddMenu() throws Exception {
        String token;
        token = getToken();        
        Product prod1 =  new Product(1L,"koseksi","bnine",10,14.5,"image",true,new Category(2L,"pp"), null);
		Product prod2 =  new Product(1L,"koseksi","bnine",10,14.5,"image",true,new Category(2L,"pp"), null);
		List<Product> listProducts = new ArrayList<>();
		listProducts.add(prod1);
		listProducts.add(prod2);				
        this.mockMvc.perform(post("/api/menus/menu")
        		.header("Authorization", "Bearer " + token)
        		.contentType("application/json")
        		.content(asJsonString(new MenuDayDto(null,listProducts))))
        		.andExpect(status().isOk());
	}

	@Test
	void testAddProductToMenu() throws Exception {
        String token;
        token = getToken();        
        Product prod =  new Product(1L,"koseksi","bnine",10,14.5,"image",true,new Category(2L,"pp"), null);
		List<Product> listProducts = new ArrayList<>();
		listProducts.add(prod);				
			
        this.mockMvc.perform(post("/api/menus/product/{id}/{prodId}",34,1)
        		.header("Authorization", "Bearer " + token)
        		.contentType("application/json")
        		.content(asJsonString(new MenuDayDto(34L,null,listProducts))))
        		.andExpect(status().isOk());
	}
	
	@Test
	void testDeleteMenuDay() throws Exception{
	       String token;
	        token = getToken();
	        this.mockMvc.perform(delete("/api/menus/{id}", 44)
			.header("Authorization", "Bearer " + token))
			.andExpect(status().isOk());
			
	}
	
	@Test
	void testGetMenuDayByDate() throws Exception{
        String token;
        token = getToken();
		MenuDayDto menuDayDto = new MenuDayDto();
		menuDayDto.setId(7L);
		menuDayDto.setDate(LocalDate.now());
		when(menuDayService.getMenuDayByDate(LocalDate.now())).thenReturn(menuDayDto);
		this.mockMvc.perform(get("/api/menus/menu/date")
        		.param("date", LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
				.header("Authorization", "Bearer " + token))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.date", is(LocalDate.now().toString())));
	}
	
	public static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
	
    public String getToken() throws Exception {
        String email = "foulen@gmail.com";
        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET.getBytes());
        List<String> roles = new ArrayList<>();
        roles.add("ADMIN");
        String access_token = JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.EXPIRE_ACCESS_TOKEN))
                .withIssuer("http://localhost:8090/api/users/login")
                .withClaim("roles", roles)
                .sign(algorithm);
        return access_token;
    }
}

