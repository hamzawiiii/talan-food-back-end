package com.talan.food.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.talan.food.dto.ReservationDto;
import com.talan.food.entities.Role;
import com.talan.food.entities.User;
import com.talan.food.services.ReservationService;
import com.talan.food.services.UserService;
import com.talan.food.util.JWTUtil;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ReservationController.class)
public class ReservationControllerTest {

    @MockBean
    private ReservationService reservationService;
    @MockBean
    UserDetailsService userDetailsService;
    @MockBean
    PasswordEncoder passwordEncoder;
    @MockBean
    UserService userService;

    @Autowired
    private MockMvc mockMvc;


    @Test
    void testGetReservation() throws Exception {
        String token;
        token = getToken();
        List<ReservationDto> reservationDtos = new ArrayList<>();
        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setId(7L);
        reservationDtos.add(reservationDto);
        when(reservationService.getAllReservation()).thenReturn(reservationDtos);
        this.mockMvc.perform(get("/api/reservations")
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(7)));
    }


    @Test
    void testGetReservationById() throws Exception {
        String token;
        token = getToken();
        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setId(7L);
        when(reservationService.getReservationById(27L)).thenReturn(reservationDto);
        this.mockMvc.perform(get("/api/reservations/GetAllById/{id}", 27)
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id", is(7)));
    }

    @Test
    void testGetAllReservationByUserId() throws Exception {
        String token;
        token = getTokenUser();
        List<ReservationDto> reservationDtos = new ArrayList<>();
        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setId(7L);
        reservationDtos.add(reservationDto);
       
        this.mockMvc.perform(get("/api/reservations/user/{iduser}/{page}", 5,1)
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk());
             

    }

    @Test
    void testGetAllReservationsByDate() throws Exception {
        String token;
        token = getToken();
        List<ReservationDto> reservationDtos = new ArrayList<>();
        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setId(7L);
        reservationDtos.add(reservationDto);
        when(reservationService.getReservationByDate(LocalDate.now())).thenReturn(reservationDtos);
        this.mockMvc.perform(get("/api/reservations/date")
                        .param("date", LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(7)));

    }

    @Test
    void testConfirmReservation() throws Exception {
        String token;
        token = getTokenUser();
        when(reservationService.annulerReservation(2L)).thenReturn(true);
        this.mockMvc.perform(delete("/api/reservations/annulation/{reservationId}", 2)
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk());

    }
	@Test
	void testReservationStatic() throws Exception{
	       String token;
	        token = getToken();
	        this.mockMvc.perform(get("/api/reservations/static")
			.header("Authorization", "Bearer " + token))
			.andExpect(status().isOk());
			
	}
	
	@Test
	void testReservationStaticMonth() throws Exception{
	       String token;
	        token = getToken();
	        this.mockMvc.perform(get("/api/reservations/static/month")
			.header("Authorization", "Bearer " + token))
			.andExpect(status().isOk());
			
	}
	@Test
	void testReservationStaticYear() throws Exception{
	       String token;
	        token = getToken();
	        this.mockMvc.perform(get("/api/reservations/static/year")
			.header("Authorization", "Bearer " + token))
			.andExpect(status().isOk());
			
	}
	@Test
	void testReservationStaticIncomes() throws Exception{
	       String token;
	        token = getToken();
	        this.mockMvc.perform(get("/api/reservations/incomes")
			.header("Authorization", "Bearer " + token))
			.andExpect(status().isOk());
			
	}
	@Test
	void testReservationStaticIncomesMonth() throws Exception{
	       String token;
	        token = getToken();
	        this.mockMvc.perform(get("/api/reservations/incomes/month")
			.header("Authorization", "Bearer " + token))
			.andExpect(status().isOk());
			
	}
	@Test
	void testReservationStaticIncomesYear() throws Exception{
	       String token;
	        token = getToken();
	        this.mockMvc.perform(get("/api/reservations/incomes/year")
			.header("Authorization", "Bearer " + token))
			.andExpect(status().isOk());
			
	}
	@Test
	void testReservationStaticIncomesUser() throws Exception{
	       String token;
	        token = getToken();
	        this.mockMvc.perform(get("/api/reservations/incomes/user")
			.header("Authorization", "Bearer " + token))
			.andExpect(status().isOk());
			
	}
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void testaddReservation() throws Exception {
        String token;
        token = getTokenUser();
        this.mockMvc.perform(post("/api/reservations")
                        .header("Authorization", "Bearer " + token)
                        .contentType("application/json")

                        .content(asJsonString((new ReservationDto(29.5, new User(4L, "ali", "bouzaien", "ali.bouzaiene@talan.com", "00000000", "53250795", new Role(2L, "USER"), false), null)))))
                .andExpect(status().isOk());
    }

	/*
	 * @Test void editReservation() throws Exception { String token; token =
	 * getTokenUser(); this.mockMvc.perform(post("/api/reservations/Edit")
	 * .header("Authorization", "Bearer " + token) .contentType("application/json")
	 * .content(asJsonString((new ReservationDto(1L, 15.5, new User(5L, "oussama",
	 * "ouss", "oussama@talan.com", "00000000", "12345678", new Role(2L, "USER"),
	 * false), null, false))))) .andExpect(status().isOk()); }
	 * 
	 * @Test void confirmReservation() throws Exception { String token; token =
	 * getTokenUser(); mockMvc.perform(post("/api/reservations/confirm")
	 * .header("Authorization", "Bearer " + token) .contentType("application/json")
	 * .content(asJsonString((new ReservationDto(1L, 15.5, new User(5L, "oussama",
	 * "ouss", "oussama@talan.com", "00000000", "12345678", new Role(2L, "USER"),
	 * false), null, false))))) .andExpect(status().isOk());
	 * 
	 * }
	 */


    @Test
    void testDeleteReservation() throws Exception {
        String token;
        token = getToken();
        this.mockMvc.perform(delete("/api/reservations/delete/{id}", 61)
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk());

    }

    public String getToken() throws Exception {
        String email = "foulen@gmail.com";
        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET.getBytes());
        List<String> roles = new ArrayList<>();
        roles.add("ADMIN");
        String access_token = JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.EXPIRE_ACCESS_TOKEN))
                .withIssuer("http://localhost:8090/api/users/login")
                .withClaim("roles", roles)
                .sign(algorithm);
        return access_token;
    }

    public String getTokenUser() throws Exception {
        String email = "foulen@gmail.com";
        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET.getBytes());
        List<String> roles = new ArrayList<>();
        roles.add("USER");
        String access_token = JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.EXPIRE_ACCESS_TOKEN))
                .withIssuer("http://localhost:8090/api/users/login")
                .withClaim("roles", roles)
                .sign(algorithm);
        return access_token;
    }

}

