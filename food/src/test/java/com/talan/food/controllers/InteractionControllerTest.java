package com.talan.food.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.talan.food.dto.InteractionDto;
import com.talan.food.entities.Role;
import com.talan.food.entities.User;
import com.talan.food.services.InteractionService;
import com.talan.food.services.UserService;
import com.talan.food.util.JWTUtil;

@ExtendWith(SpringExtension.class)
@WebMvcTest(InteractionController.class)
public class InteractionControllerTest {

    @MockBean
    InteractionService interactionService;
    @MockBean
    UserDetailsService userDetailsService;
    @MockBean
    PasswordEncoder passwordEncoder;
    @MockBean
    UserService userService;
    @Autowired
    private MockMvc mockMvc;

    @Test
    void testGetInteraction() throws Exception {
        String token;
        token = getToken();
        InteractionDto interactionDto = new InteractionDto();
        interactionDto.setId(7L);
        interactionDto.setDescription("quantité reduite");
        List<InteractionDto> interactions = new ArrayList<>();
        interactions.add(interactionDto);
        when(interactionService.getAllInteraction()).thenReturn(interactions);
        this.mockMvc.perform(get("/api/interactions/All")
        	.header("Authorization", "Bearer " + token))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].description", is("quantité reduite")));
    }
    
    @Test
    void testGetAllReclamations() throws Exception {
        String token;
        token = getToken();
        InteractionDto interactionDto = new InteractionDto();
        interactionDto.setId(7L);
        interactionDto.setType("Reclamation");
        List<InteractionDto> interactions = new ArrayList<>();
        interactions.add(interactionDto);
        when(interactionService.getInteractionByReclamation()).thenReturn(interactions);
        this.mockMvc.perform(get("/api/interactions/reclamations")
        	.header("Authorization", "Bearer " + token))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].type", is("Reclamation")));
    }
    
    @Test
    void testGetAllSuggestions() throws Exception {
        String token;
        token = getToken();
        InteractionDto interactionDto = new InteractionDto();
        interactionDto.setId(7L);
        interactionDto.setType("Suggestion");
        List<InteractionDto> interactions = new ArrayList<>();
        interactions.add(interactionDto);
        when(interactionService.getInteractionBySuggestion()).thenReturn(interactions);
        this.mockMvc.perform(get("/api/interactions/suggestions")
        	.header("Authorization", "Bearer " + token))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].type", is("Suggestion")));
    }
    
    @Test
    void testGetAllNotes() throws Exception {
        String token;
        token = getToken();
        InteractionDto interactionDto = new InteractionDto();
        interactionDto.setId(7L);
        interactionDto.setType("Note");
        List<InteractionDto> interactions = new ArrayList<>();
        interactions.add(interactionDto);
        when(interactionService.getInteractionByNote()).thenReturn(interactions);
        this.mockMvc.perform(get("/api/interactions/notes")
        	.header("Authorization", "Bearer " + token))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].type", is("Note")));
    }
    

    @Test
    void testGetInteractionById() throws Exception {
        String token;
        token = getToken();
        InteractionDto interactionDto = new InteractionDto();
        interactionDto.setId(7L);
        interactionDto.setDescription("quantité reduite");
        when(interactionService.getInteractionById(7L)).thenReturn(interactionDto);
        this.mockMvc.perform(get("/api/interactions/GetById/{id}", 7)
        	.header("Authorization", "Bearer " + token))
            .andExpect(status().isOk())
            .andExpect(jsonPath("description", is("quantité reduite")));
    }
          
    @Test
    void testaddInteraction() throws Exception {
        String token;
        token = getTokenUser();
        this.mockMvc.perform(post("/api/interactions")
        		.header("Authorization", "Bearer " + token)
                .contentType("application/json")
                .content(asJsonString((new InteractionDto("Reclamation","quantité reduite",3, new User(4L,"ali", "bouzaien","ali.bouzaiene@talan.com","00000000", "53250795", new Role(2L,"USER"), false) )))))
        		.andExpect(status().isOk());
    }
    
    @Test
    void testClientAnswer() throws Exception {
        String token;
        token = getToken();
        String message="hello";
        this.mockMvc.perform(get("/api/interactions/reclamation/response/{idRec}",1)
        		.param("message", message)
        	.header("Authorization", "Bearer " + token))
            .andExpect(status().isOk());
          
    }
    
    @Test
    void testResponseSuggession() throws Exception {
        String token;
        token = getToken();        
        this.mockMvc.perform(get("/api/interactions/suggession/response/{idRec}",1)        		
        	.header("Authorization", "Bearer " + token))
            .andExpect(status().isOk());
          
    }
    
    @Test
	void testDeleteInterraction() throws Exception{
	       String token;
	        token = getToken();
	        this.mockMvc.perform(delete("/api/interactions/{id}", 163)
			.header("Authorization", "Bearer " + token))
			.andExpect(status().isOk());
			
	}

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public String getToken() throws Exception {
        String email = "foulen@gmail.com";
        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET.getBytes());
        List<String> roles = new ArrayList<>();
        roles.add("ADMIN");
        String access_token = JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.EXPIRE_ACCESS_TOKEN))
                .withIssuer("http://localhost:8090/api/users/login")
                .withClaim("roles", roles)
                .sign(algorithm);
        return access_token;
    }
    public String getTokenUser() throws Exception {
        String email = "foulen@gmail.com";
        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET.getBytes());
        List<String> roles = new ArrayList<>();
        roles.add("USER");
        String access_token = JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.EXPIRE_ACCESS_TOKEN))
                .withIssuer("http://localhost:8090/api/users/login")
                .withClaim("roles", roles)
                .sign(algorithm);
        return access_token;
    }
}
