package com.talan.food.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.talan.food.dto.OrderDto;
import com.talan.food.entities.Category;
import com.talan.food.entities.Product;
import com.talan.food.entities.Reservation;
import com.talan.food.entities.Role;
import com.talan.food.entities.User;
import com.talan.food.services.OrderService;
import com.talan.food.services.UserService;
import com.talan.food.util.JWTUtil;


@ExtendWith(SpringExtension.class)
@WebMvcTest(OrderController.class)
public class OrderControllerTest {

	@MockBean
	private OrderService orderService;
	@MockBean
	UserDetailsService userDetailsService;
	@MockBean
	PasswordEncoder passwordEncoder;
	@MockBean
	UserService userService;
	@Autowired
	private MockMvc mockMvc;

	@Test
	void testGetAllOrders() throws Exception {
        String token;
        token = getToken();
		OrderDto orderDto = new OrderDto();
		User user=new User();
		user.setId(1L);
		orderDto.setId(7L);
		List<OrderDto> orders = new ArrayList<>();
		orders.add(orderDto);
		when(orderService.getAllOrders()).thenReturn(orders);
        this.mockMvc.perform(get("/api/ordres")
		.header("Authorization", "Bearer " + token))
		.andExpect(status().isOk())
		.andExpect( jsonPath("$[0].id", is(7)));
	}
	@Test
	void testFindOrderById() throws Exception {
        String token;
        token = getToken();
		OrderDto orderDto = new OrderDto();
		orderDto.setId(7L);
		orderDto.setQuantity(17);
		when(orderService.findOrderById(7L)).thenReturn(orderDto);
		this.mockMvc.perform(get("/api/ordres/{id}", 7)
		.header("Authorization", "Bearer " + token))
		.andExpect(status().isOk())
		.andExpect( jsonPath("quantity", is(17)));
	}
	
	@Test
	void testGetOrderByReservation() throws Exception {
        String token;
        token = getToken();
		OrderDto orderDto = new OrderDto();
		orderDto.setId(7L);
		orderDto.setQuantity(17);
		List<OrderDto> orders = new ArrayList<>();
		orders.add(orderDto);
		when(orderService.getByResrvationId(7L)).thenReturn(orders);
        this.mockMvc.perform(get("/api/ordres/reservation/{id}", 7)
		.header("Authorization", "Bearer " + token))
		.andExpect(status().isOk())
		.andExpect( jsonPath("$[0].quantity", is(17)));
	}
	@Test
	void testAddOrder() throws Exception{
		String token;
	    token = getTokenUser();
	    Product prod = new Product(1L,"koseksi","bnine",10,14.5,"image",true,new Category(2L,"pp"), null);
	    Reservation reservation = new Reservation(1L,15.5,new User(5L,"oussama", "ouss","oussama@talan.com","00000000", "12345678", new Role(2L,"USER"), false),null, false, false);
		this.mockMvc.perform(post("/api/ordres")
		.header("Authorization", "Bearer " + token)
		.contentType("application/json")
		.content(asJsonString(new OrderDto(prod,1,reservation))))		
		.andExpect(status().isOk());
	}	
	
	@Test
	void testDeleteOrder() throws Exception{
	       String token;
	        token = getToken();
	        this.mockMvc.perform(delete("/api/ordres/{id}", 15)
			.header("Authorization", "Bearer " + token))
			.andExpect(status().isOk());
			
	}

	@Test
	void testGetOrderUser() throws Exception{
	       String token;
	        token = getToken();
	        this.mockMvc.perform(get("/api/ordres/user/{id}", 1)
			.header("Authorization", "Bearer " + token))
			.andExpect(status().isOk());
			
	}

	@Test
	void testGetProductQuantity() throws Exception{
	       String token;
	        token = getToken();
	        this.mockMvc.perform(get("/api/ordres/product/quantity")
			.header("Authorization", "Bearer " + token))
			.andExpect(status().isOk());
			
	}
	public static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}

    public String getToken() throws Exception {
        String email = "foulen@gmail.com";
        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET.getBytes());
        List<String> roles = new ArrayList<>();
        roles.add("ADMIN");
        String access_token = JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.EXPIRE_ACCESS_TOKEN))
                .withIssuer("http://localhost:8090/api/users/login")
                .withClaim("roles", roles)
                .sign(algorithm);
        return access_token;
    }
    
    public String getTokenUser() throws Exception {
        String email = "foulen@gmail.com";
        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET.getBytes());
        List<String> roles = new ArrayList<>();
        roles.add("USER");
        String access_token = JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.EXPIRE_ACCESS_TOKEN))
                .withIssuer("http://localhost:8090/api/users/login")
                .withClaim("roles", roles)
                .sign(algorithm);
        return access_token;
    }
}
