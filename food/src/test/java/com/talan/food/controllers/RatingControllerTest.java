package com.talan.food.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.talan.food.dto.RatingDto;
import com.talan.food.entities.Product;
import com.talan.food.entities.User;
import com.talan.food.services.RatingService;
import com.talan.food.services.UserService;
import com.talan.food.util.JWTUtil;

@ExtendWith(SpringExtension.class)
@WebMvcTest(RatingController.class)
public class RatingControllerTest {

	@MockBean
	RatingService ratingService;
	@MockBean
	UserDetailsService userDetailsService;
	@MockBean
	PasswordEncoder passwordEncoder;
	@MockBean
	UserService userService;
	@Autowired
	private MockMvc mockMvc;

	@Test
	void addRating() throws Exception {
		User user = new User();
		Product product = new Product();
		user.setId(4L);
		product.setId(1L);
        String token;
        token = getTokenUser();	
	    this.mockMvc.perform(post("/api/products/ratings")
	    		.header("Authorization", "Bearer " + token)
                .contentType("application/json")
                .content(asJsonString((new RatingDto(5.5,user,product)))))
        		.andExpect(status().isOk());
	}
	@Test
	void RatingProductByUser() throws Exception {
		User user = new User();
		Product product = new Product();
		user.setId(4L);
		product.setId(1L);
        String token;
        token = getTokenUser();	
	    this.mockMvc.perform(get("/api/products/ratings/{idUser}/{idProduct}",4,1)
	    		.header("Authorization", "Bearer " + token)
	    		 .contentType("application/json"))
        		.andExpect(status().isOk());
	}
	@Test
	void getRating() throws Exception {
        String token;
        token = getToken();	
		this.mockMvc.perform(get("/api/products/ratings/{id}", 1)
				.header("Authorization", "Bearer " + token))
				.andExpect(status().isOk());
	}
	public static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e); 
	    }
	}
	
    public String getToken() throws Exception {
        String email = "foulen@gmail.com";
        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET.getBytes());
        List<String> roles = new ArrayList<>();
        roles.add("ADMIN");
        String access_token = JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.EXPIRE_ACCESS_TOKEN))
                .withIssuer("http://localhost:8090/api/users/login")
                .withClaim("roles", roles)
                .sign(algorithm);
        return access_token;
    }
    
    public String getTokenUser() throws Exception {
        String email = "foulen@gmail.com";
        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET.getBytes());
        List<String> roles = new ArrayList<>();
        roles.add("USER");
        String access_token = JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.EXPIRE_ACCESS_TOKEN))
                .withIssuer("http://localhost:8090/api/users/login")
                .withClaim("roles", roles)
                .sign(algorithm);
        return access_token;
    }
}

