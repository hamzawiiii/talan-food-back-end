package com.talan.food.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.talan.food.dto.UserDto;
import com.talan.food.entities.Role;
import com.talan.food.services.impl.UserServiceImpl;
import com.talan.food.util.JWTUtil;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
class UserControllerTest {

    @MockBean
    UserServiceImpl userService;
    @MockBean
    PasswordEncoder passwordEncoder;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
	HttpServletRequest request;


    @Test
    void signup() throws Exception {
        Role role = new Role();
        role.setId(2L);

        UserDto userDto = new UserDto(4L,"ali", "Ben Falten", "foulen@gmail.com", "foulenbenfalten", "21001920", role, false);

        given(userService.signup(userDto)).willReturn(userDto);
        this.mockMvc.perform(post("/api/users/signup")
                        .contentType("application/json")
                        .content(asJsonString((userDto))))
                .andExpect(status().isCreated());
    }

    @Test
    void itShouldNotSignup() throws Exception {
        Role role = new Role();
        role.setId(2L);
        UserDto userDto = new UserDto(4L,"ali", "Ben Falten", "foulen@gmail.com", "foulenbenfalten", "21001920", role, false);

        given(userService.signup(any())).willReturn(null);
        this.mockMvc.perform(post("/api/users/signup")
                        .contentType("application/json")
                        .content(asJsonString((userDto))))
                .andExpect(status().isNotAcceptable());
    }


    @Test
    void getUserById() throws Exception {
        String token;
        token = getToken();
        Role role = new Role();
        role.setId(2L);
        UserDto userDto = new UserDto(4L,"ali", "Ben Falten", "foulen@gmail.com", "foulenbenfalten", "21001920", role, false);

        given(userService.getUserById(4L)).willReturn(userDto);
        this.mockMvc.perform(get("/api/users/{userId}", 4)
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is("ali")));
        
        this.mockMvc.perform(get("/api/users/{userId}", 400)
                .header("Authorization", "Bearer " + token))
        .andExpect(status().isNoContent());
    }


    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String getToken() throws Exception {
        String email = "foulen@gmail.com";
        Algorithm algorithm = Algorithm.HMAC256(JWTUtil.SECRET.getBytes());
        List<String> roles = new ArrayList<>();
        roles.add("ADMIN");
        String access_token = JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtil.EXPIRE_ACCESS_TOKEN))
                .withIssuer("http://localhost:8090/api/users/login")
                .withClaim("roles", roles)
                .sign(algorithm);
        return access_token;
    }

}
