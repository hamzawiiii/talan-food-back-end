package com.talan.food.controllers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.talan.food.dto.SubscriptionDto;
import com.talan.food.services.SubscriptionService;

@SpringBootTest
@AutoConfigureMockMvc

class SubscriptionControllerTest {

	@MockBean
	SubscriptionService subscriptionService;

	@Autowired
	MockMvc mockMvc;

	@Test
	void getSubscriptionStatus() throws Exception {
		when(subscriptionService.getSubscriptionStatus(3L, 2L)).thenReturn(true);
		this.mockMvc.perform(get("/api/subscriptions/{productid}/{userid}", 3L, 2L)).andExpect(status().isOk())
				.andExpect(content().string("true"));

	}

	@Test
	void countNotSeenNotifications() throws Exception {
		when(subscriptionService.countNotSeenNotifications(2L, false)).thenReturn(4);
		this.mockMvc.perform(get("/api/subscriptions/notSeen/{id}", 2L)).andExpect(status().isOk())
				.andExpect(content().string("4"));

	}

	@Test
	void getSubscriptions() throws Exception {

		SubscriptionDto subDto = new SubscriptionDto();
		subDto.setId(2L);
		List<SubscriptionDto> userSubs = new ArrayList<>();
		userSubs.add(subDto);
		when(subscriptionService.getSubscriptions(2L)).thenReturn(userSubs);
		mockMvc.perform(get("/api/subscriptions/user/{id}", 2L)).andExpect(status().isOk()).andExpect(
				content().json("[{'id':2,'user':null,'product':null,'seen':false,'menuDate':null,'activated':false}]"));

	}

	@Test
	void testChangeSubscriptionStatus() throws Exception {

		SubscriptionDto subscriptionDto = new SubscriptionDto();
		subscriptionDto.setId(2L);

		when(subscriptionService.changeSubscriptionStatus(subscriptionDto)).thenReturn(subscriptionDto);

		ResultActions result = mockMvc.perform(post("/api/subscriptions").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(subscriptionDto)));

		result.andExpect(status().isOk()).andExpect(
				content().json("{'id':2,'user':null,'product':null,'seen':false,'menuDate':null,'activated':false}"));
	}

	@Test
	void changeToSeen() throws Exception {

		SubscriptionDto subscriptionDto = new SubscriptionDto();
		subscriptionDto.setId(2L);
		subscriptionDto.setSeen(true);

		when(subscriptionService.changeToSeen(2L)).thenReturn(subscriptionDto);

		mockMvc.perform(patch("/api/subscriptions/seen/{id}", 2L)).andExpect(status().isOk()).andExpect(
				content().json("{'id':2,'user':null,'product':null,'seen':true,'menuDate':null,'activated':false}"));
	}

}
