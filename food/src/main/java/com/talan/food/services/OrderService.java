package com.talan.food.services;

import java.util.List;

import com.talan.food.dto.CountProduct;
import com.talan.food.dto.OrderDto;
import com.talan.food.entities.Order;


public interface OrderService {
	
	public List<OrderDto> getAllOrders ();
	public OrderDto findOrderById (Long id);
	public OrderDto addOrder(OrderDto order);
	public void deleteOrder (Long id);
	public List<OrderDto> getByResrvationId (Long id);
	public List<Order> getAllOrdersByUser (Long id);
	public List<CountProduct> getQuantityByProduct ();

}
