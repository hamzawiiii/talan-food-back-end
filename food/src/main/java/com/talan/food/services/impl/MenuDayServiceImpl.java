package com.talan.food.services.impl;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.talan.food.dto.MenuDayDto;
import com.talan.food.dto.ProductDto;
import com.talan.food.dto.SubscriptionDto;
import com.talan.food.entities.MenuDay;
import com.talan.food.entities.Product;
import com.talan.food.entities.Subscription;
import com.talan.food.helpers.ModelMapperConverter;
import com.talan.food.repositories.MenuRepo;
import com.talan.food.repositories.SubscriptionRepo;
import com.talan.food.services.MenuDayService;
import com.talan.food.services.ProductService;
import com.talan.food.services.SubscriptionService;

@Service
public class MenuDayServiceImpl implements MenuDayService {

    @Autowired
    private MenuRepo menuRepo;

    @Autowired
    private ProductService productService;
    
    @Autowired
	private SubscriptionRepo subscriptionRepo;

	@Autowired
	private SubscriptionService subscriptionService;

    /**
     * @Override public MenuDayDto saveMenuDay(MenuDayDto menu) {
     * return ModelMapperConverter.map(menuRepo.save(ModelMapperConverter.map(menu, MenuDay.class)), MenuDayDto.class);
     * }
     */

    @SuppressWarnings("finally")
    @Override
    public MenuDayDto saveMenuDay(MenuDayDto menu) {

    	LocalDate menuDate = menu.getDate();
		List<MenuDayDto> listMenus = new ArrayList<>();
		listMenus = getAllMenus();
		List<Product> menuProducts = menu.getListProducts();

		try {
			if (!listMenus.isEmpty()) {
				for (int i = 0; i < listMenus.size(); i++) {
					if (menuDate.equals(listMenus.get(i).getDate())) {
						deleteMenuDayById(listMenus.get(i).getId());
					}
				}
			}

			for (Product product : menuProducts) {
				List<SubscriptionDto> subscriptions = subscriptionService.findByProductId(product.getId());
				if (!subscriptions.isEmpty()) {
					if (subscriptions.get(0).getMenuDate() == null) {
						subscriptions.get(0).setMenuDate(menuDate);
						subscriptionRepo.save(ModelMapperConverter.map(subscriptions.get(0), Subscription.class));

					} else {
						Subscription sub1 = new Subscription();
						sub1.setActivated(true);
						sub1.setMenuDate(menuDate);
						sub1.setProduct(product);
						sub1.setUser(subscriptions.get(0).getUser());
						subscriptionRepo.save(sub1);

					}
				}

			}
		} catch (Exception e) {
			e.fillInStackTrace();
			
		} finally {
			 ModelMapperConverter.map(menuRepo.save(ModelMapperConverter.map(menu, MenuDay.class)),
					MenuDayDto.class);
			 
			 for (Product product : menuProducts) {
				 
				 subscriptionService.changeUserNotificationsToVisible(product.getId());
				
			}
			 
			 
			 
			 return menu;
		}
    }


    @Override
    public MenuDayDto getMenuDayById(Long id) {
        return ModelMapperConverter.map(menuRepo.findById(id), MenuDayDto.class);
    }

    @Override
    public List<MenuDayDto> getAllMenus() {
        return ModelMapperConverter.mapAll(menuRepo.findAllByOrderByDate(), MenuDayDto.class);
    }


    @Override
    public void deleteMenuDayById(Long id) {
        menuRepo.deleteById(id);
    }

    @Override
    @Transactional
    public MenuDayDto addProductToMenuDay(Long menuId, Long prodId) {

        ProductDto productDto = productService.getProductById(prodId);
        Product product = ModelMapperConverter.map(productDto, Product.class);

        MenuDay menu = menuRepo.getById(menuId);
        menu.getListProducts().add(product);
        menuRepo.save(menu);

        return ModelMapperConverter.map(menuRepo.save(menu), MenuDayDto.class);
    }

    @Override
    public List<Product> getMenuProducts(Long menuId) {
        MenuDayDto menu = ModelMapperConverter.map(menuRepo.findById(menuId), MenuDayDto.class);
        return menu.getListProducts();

    }

    @Override
    public MenuDayDto getMenuDayByDate(LocalDate date) {
        return ModelMapperConverter.map(menuRepo.findByDate(date), MenuDayDto.class);
    }


}
