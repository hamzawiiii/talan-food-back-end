package com.talan.food.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.talan.food.dto.SubscriptionDto;
import com.talan.food.entities.Subscription;
import com.talan.food.entities.User;
import com.talan.food.helpers.ModelMapperConverter;
import com.talan.food.repositories.SubscriptionRepo;
import com.talan.food.repositories.UserRepo;
import com.talan.food.services.SubscriptionService;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

	@Autowired
	SubscriptionRepo subscriptionRepo;

	@Autowired
	UserRepo userRepo;

	@Override
	public SubscriptionDto changeSubscriptionStatus(SubscriptionDto subscriptionDto) {

		List<Subscription> subscriptions = subscriptionRepo
				.findByProductIdAndUserId(subscriptionDto.getProduct().getId(), subscriptionDto.getUser().getId());

		if (subscriptions.isEmpty()) {
			subscriptionDto.setActivated(true);
			return ModelMapperConverter.map(
					subscriptionRepo.save(ModelMapperConverter.map(subscriptionDto, Subscription.class)),
					SubscriptionDto.class);
		}

		for (Subscription productSubscription : subscriptions) {
			productSubscription.setActivated(!productSubscription.isActivated());

		}

		ModelMapperConverter.map(subscriptionRepo.saveAll(subscriptions), SubscriptionDto.class);
		return ModelMapperConverter.map(subscriptions.get(0), SubscriptionDto.class);

	}

	@Override
	public boolean getSubscriptionStatus(Long productId, Long userId) {

		Subscription subscription = subscriptionRepo.findFirst1ByProductIdAndUserId(productId, userId);
		if (subscription == null)
			return false;

		return subscription.isActivated();
	}

	@Override
	public List<SubscriptionDto> findByProductId(Long productId) {

		return ModelMapperConverter.mapAll(subscriptionRepo.findByProductIdAndActivated(productId, true),
				SubscriptionDto.class);
	}

	@Override
	public int countNotSeenNotifications(Long userId, boolean seen) {

		return subscriptionRepo.countByMenuDateNotNullAndUserIdAndSeen(userId, false);
	}

	@Override
	public List<SubscriptionDto> getSubscriptions(Long userId) {

		return ModelMapperConverter.mapAll(subscriptionRepo.findByMenuDateNotNullAndUserIdOrderByIdDesc(userId),
				SubscriptionDto.class);
	}

	@Override
	public SubscriptionDto changeToSeen(Long id) {
		Subscription subscription = subscriptionRepo.getById(id);
		subscription.setSeen(true);

		return ModelMapperConverter.map(subscriptionRepo.save(subscription), SubscriptionDto.class);
	}

	@Override
	public List<User> changeUserNotificationsToVisible(Long productId) {

		List<User> users = this.findByProductId(productId).stream().map(subscription -> subscription.getUser())
				.distinct().collect(Collectors.toList());

		for (User user : users) {
			user.setNotificationsHidden(false);
			userRepo.save(user);
		}

		return users;
	}

}
