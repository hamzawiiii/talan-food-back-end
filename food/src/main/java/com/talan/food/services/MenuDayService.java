package com.talan.food.services;


import com.talan.food.dto.MenuDayDto;
import com.talan.food.entities.Product;

import java.time.LocalDate;
import java.util.List;

public interface MenuDayService {

	
	MenuDayDto saveMenuDay(MenuDayDto menu);
	
	MenuDayDto getMenuDayById(Long id);
	
	void deleteMenuDayById(Long id);
	
	List<MenuDayDto> getAllMenus();
	
	MenuDayDto addProductToMenuDay(Long prodId, Long menuId);
	
	List<Product> getMenuProducts (Long menId);
	
	MenuDayDto getMenuDayByDate(LocalDate date);
	
	
}
