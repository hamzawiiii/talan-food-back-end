package com.talan.food.services;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.talan.food.dto.CategoryDto;
import com.talan.food.dto.ProductDto;
import com.talan.food.entities.Product;

public interface ProductService {

	ProductDto saveProduct(ProductDto p,MultipartFile productImage)throws IOException ;
	
	void deleteProductById(Long id);

	ProductDto getProductById(Long id);

	List<ProductDto> getAllProducts();
	
	List<ProductDto> getProductsByCategory(CategoryDto cat);
	
	List<ProductDto> getByCategoryId(Long id);
	
	void saveProfileImage(Product product, MultipartFile productImage)throws IOException;
	String setProfileImageUrl(String productName);
	
	ProductDto updateProduct(ProductDto p);

}
