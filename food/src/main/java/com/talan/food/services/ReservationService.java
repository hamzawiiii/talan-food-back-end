package com.talan.food.services;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;

import com.talan.food.dto.Count;
import com.talan.food.dto.MonthIncomes;
import com.talan.food.dto.MonthReservation;
import com.talan.food.dto.Price;
import com.talan.food.dto.ReservationDto;
import com.talan.food.dto.ReservationStaticDto;
import com.talan.food.dto.UserIncome;
import com.talan.food.dto.YearIncomes;
import com.talan.food.dto.YearReservation;


public interface ReservationService {
	public List<ReservationDto> getAllReservation();
	public ReservationDto getReservationById(Long id);
	public ReservationDto addReservation(ReservationDto reservationDto);
	public ReservationDto editReservation(ReservationDto reservationDto);
	public void deleteReservationById(Long id);
	public Page<ReservationDto>getReservationByUserId(Long id , int page );
	public ReservationDto confirmReservation(ReservationDto reservationDto);
	public List<ReservationDto> getReservationByDate(LocalDate date);
	public boolean annulerReservation(Long id);
	public List<ReservationStaticDto> getReservationByDateAsc();
	public List<Count> getcountReservation();
	public List<Price> getIncomesByDate();
	public List<MonthIncomes> getIncomesByMonth();
	public List<YearIncomes> getIncomesByYear();
	public List<UserIncome> getIncomesByUser();
	public List<MonthReservation> getReservationByMonth();
	public List<YearReservation> getReservationByYear();
	public int countByIsSeen(boolean isSeen);
	public ReservationDto changeToSeen(Long id);


	
	
}
