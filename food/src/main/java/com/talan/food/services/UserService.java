package com.talan.food.services;

import com.talan.food.dto.UserDto;
import com.talan.food.entities.User;

public interface UserService {
    public UserDto signup(UserDto userDto);
    public UserDto getUserById(Long userId);
    public UserDto getUserByEmail(String email);
    public UserDto changeNotificationsVisibility(boolean hidden, Long id);
    public User showAdminNotifications();
}
