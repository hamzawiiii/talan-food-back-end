package com.talan.food.services.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.talan.food.dto.Count;
import com.talan.food.dto.MonthIncomes;
import com.talan.food.dto.MonthReservation;
import com.talan.food.dto.Price;
import com.talan.food.dto.ReservationDto;
import com.talan.food.dto.ReservationStaticDto;
import com.talan.food.dto.UserIncome;
import com.talan.food.dto.YearIncomes;
import com.talan.food.dto.YearReservation;
import com.talan.food.entities.Order;
import com.talan.food.entities.Product;
import com.talan.food.entities.Reservation;
import com.talan.food.entities.User;
import com.talan.food.helpers.ModelMapperConverter;
import com.talan.food.repositories.OrderRepo;
import com.talan.food.repositories.ProductRepo;
import com.talan.food.repositories.ReservationRepo;
import com.talan.food.repositories.UserRepo;
import com.talan.food.services.ReservationService;
import com.talan.food.services.UserService;


@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    ReservationRepo reservationRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    OrderRepo orderrepo;
    @Autowired
    ProductRepo productRepo;
    @Autowired
    UserService userService;
    @Autowired
    private JavaMailSender javaMailSender;
    static final String BONJOUR = "Bonjour Gourmet ! Le collaborateur ";
    static final String EMAIL = "hamza.bouachir@talan.com";

    @Override
    public List<ReservationDto> getAllReservation() {
        return ModelMapperConverter.mapAll(reservationRepo.findAllByOrderByIdDesc(), ReservationDto.class);
    }

    @Override
    public List<ReservationDto> getReservationByDate(LocalDate date) {
        return ModelMapperConverter.mapAll(reservationRepo.findAllByDateOrderById(date), ReservationDto.class);
    }

    @Override
    public ReservationDto getReservationById(Long id) {
        return ModelMapperConverter.map(reservationRepo.findById(id), ReservationDto.class);
    }

    @Override
    public ReservationDto addReservation(ReservationDto reservationDto) {
        /******evoie par id*****/
        Optional<User> userOptional = userRepo.findById(reservationDto.getUser().getId());
        reservationDto.setUser(userOptional.isPresent() ? userOptional.get() : null);
        /******fin evoie par id*****/

        Reservation reservation = ModelMapperConverter.map(reservationDto, Reservation.class);
        reservationRepo.save(reservation);
        userService.showAdminNotifications();
        
        return ModelMapperConverter.map(reservation, ReservationDto.class);

    }

    @Override
    public ReservationDto editReservation(ReservationDto reservationDto) {


        /******evoie par id*****/
        Optional<User> userOptional = userRepo.findById(reservationDto.getUser().getId());
        reservationDto.setUser(userOptional.isPresent() ? userOptional.get() : null);
        /******fin evoie par id*****/

        Reservation editedReservation = ModelMapperConverter.map(reservationDto, Reservation.class);
        reservationRepo.save(editedReservation);

        //Debut traitement envoie Mail

        SimpleMailMessage sendReservation = new SimpleMailMessage();
        sendReservation.setTo(EMAIL);
        sendReservation.setText(BONJOUR + " " + editedReservation.getUser().getFirstName() + " " + editedReservation.getUser().getLastName() + " " + "a edité sa reservation numéro" + " " + editedReservation.getId());
        sendReservation.setSubject("Reservation pour le Collaborateur : " + editedReservation.getUser().getFirstName() + " " + editedReservation.getUser().getLastName());
        javaMailSender.send(sendReservation);
        //Fin traitement envoie Mail
        return ModelMapperConverter.map(editedReservation, ReservationDto.class);
    }


    @Override
    public void deleteReservationById(Long id) {
        reservationRepo.deleteById(id);

    }


	@Override
	public Page<ReservationDto> getReservationByUserId(Long id, int page) {
		return reservationRepo.findByUserIdOrderByIdDesc(id, PageRequest.of(page, 7))
				.map(entity -> ModelMapperConverter.map(entity, ReservationDto.class));
	}



    @Override
    public ReservationDto confirmReservation(ReservationDto reservationDto) {

        /******evoie par id*****/
        Optional<User> userOptional = userRepo.findById(reservationDto.getUser().getId());
        reservationDto.setUser(userOptional.isPresent() ? userOptional.get() : null);
        /******fin evoie par id*****/

        Reservation confirmedReservation = ModelMapperConverter.map(reservationDto, Reservation.class);
        confirmedReservation.setConfirmed(true);
        reservationRepo.save(confirmedReservation);
        //Debut traitement envoie Mail lorsque l'utilisateur clique sur confirmer ma reservation apres avoir bien sur terminer tout les commandes
        SimpleMailMessage sendReservation = new SimpleMailMessage();
        sendReservation.setTo(EMAIL);
        sendReservation.setText(BONJOUR + " " + confirmedReservation.getUser().getFirstName() + " " + confirmedReservation.getUser().getLastName() + " " + "a créé une nouvelle réservation sous le numero" + " " + confirmedReservation.getId());
        sendReservation.setSubject("Reservation pour le collaborateur : " +" " + confirmedReservation.getUser().getFirstName() + " " + confirmedReservation.getUser().getLastName());
        javaMailSender.send(sendReservation);
        //Fin traitement envoie Mail
        return ModelMapperConverter.map(confirmedReservation, ReservationDto.class);
    }

    @Transactional
    @Override
    public boolean annulerReservation(Long id) {
        boolean result = false;
        Reservation reservationAnnuler = reservationRepo.getById(id);
        LocalDate dateInitiale = reservationAnnuler.getDate();
        LocalDate actualDate = LocalDate.now();

        if ((actualDate.isBefore(dateInitiale.minusDays((long) (0.5))))) {
            List<Order> orders = orderrepo.findOrderByReservationId(id);
            for (int i = 0; i < orders.size(); i++) {
                int annuledquatitity = orders.get(i).getQuantity();
                int disponibleQuantity = orders.get(i).getProduct().getQuantity();
                int newquantity = disponibleQuantity + annuledquatitity;
                Product p = orders.get(i).getProduct();
                productRepo.getById(p.getId()).setQuantity(newquantity);
                orderrepo.deleteById(orders.get(i).getId());

            }
            SimpleMailMessage annulerReservation = new SimpleMailMessage();
            annulerReservation.setTo(EMAIL);
            annulerReservation.setSubject("Annulation de la réservation du collaborateur : " +" " + reservationAnnuler.getUser().getFirstName() + " " + reservationAnnuler.getUser().getLastName());
            annulerReservation.setText(BONJOUR + " " + reservationAnnuler.getUser().getFirstName() + " " + reservationAnnuler.getUser().getLastName() + " " + "a annulé la réservation numéro " + " " + reservationAnnuler.getId() + " " + "prévue pour le " + " " + reservationAnnuler.getDate());
            reservationRepo.delete(reservationAnnuler);
            javaMailSender.send(annulerReservation);

            result = true;
        } else {
            result = false;
        }

        return result;
    }

	@Override
	public List<ReservationStaticDto> getReservationByDateAsc() {

		return ModelMapperConverter.mapAll(
				reservationRepo.findAllByOrderByDateAsc().stream().distinct().collect(Collectors.toList()),
				ReservationStaticDto.class);

	}

	@Override
	public List<Count> getcountReservation() {
		
		return reservationRepo.getcountReservationByDate();
	}

	@Override
	public List<Price> getIncomesByDate() {
		
		return reservationRepo.getPriceReservationByDate();
	}

	@Override
	public List<UserIncome> getIncomesByUser() {
		return reservationRepo.getPriceReservationByUser();
	}

	@Override
	public List<MonthIncomes> getIncomesByMonth() {
		return reservationRepo.getIncomeByMonth();
	}

	@Override
	public List<MonthReservation> getReservationByMonth() {
		return reservationRepo.getReservationByMonth();
	}

	@Override
	public List<YearIncomes> getIncomesByYear() {
		return reservationRepo.getIncomeByYear();
	}

	@Override
	public List<YearReservation> getReservationByYear() {
		return reservationRepo.getReservationByYear();
	}

	@Override
	public int countByIsSeen(boolean isSeen) {
		
		return reservationRepo.countByIsSeen(isSeen);
	}

	@Override
	public ReservationDto changeToSeen(Long id) {
		Reservation reservation=reservationRepo.getById(id);
		reservation.setSeen(true);
		
		return ModelMapperConverter.map(reservationRepo.save(reservation), ReservationDto.class);
	}

}
