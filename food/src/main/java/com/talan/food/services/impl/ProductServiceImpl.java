package com.talan.food.services.impl;

import com.talan.food.dto.CategoryDto;
import com.talan.food.dto.ProductDto;
import com.talan.food.entities.Category;
import com.talan.food.entities.Product;
import com.talan.food.helpers.ModelMapperConverter;
import com.talan.food.repositories.ProductRepo;
import com.talan.food.services.ProductService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.transaction.Transactional;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;


@Transactional
@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepo productRepo;

    public ProductServiceImpl(ProductRepo productRepo) {
        super();
        this.productRepo = productRepo;
    }

    @Override
    public ProductDto saveProduct(ProductDto product, MultipartFile productImage) throws IOException {
        if (productImage != null && !productImage.isEmpty()) {

            Path userFolder = Paths.get(System.getProperty("user.home") + "/images/" + product.getName()).toAbsolutePath()
                    .normalize();
            if (!Files.exists(userFolder)) {
                Files.createDirectories(userFolder);
            }
            Files.deleteIfExists(Paths.get(userFolder + product.getName() + "." + "jpg"));
            Files.copy(productImage.getInputStream(), userFolder.resolve(product.getName() + "." + "jpg"),
                    REPLACE_EXISTING);
            product.setImage(setProfileImageUrl(product.getName()));

        } else {
            product.setImage(productRepo.getById(product.getId()).getImage());
        }
        return ModelMapperConverter.map(productRepo.save(ModelMapperConverter.map(product, Product.class)), ProductDto.class);
    }

    @Override
    public void deleteProductById(Long id) {
        productRepo.deleteById(id);
    }

    @Override
    public ProductDto getProductById(Long id) {
        return ModelMapperConverter.map(productRepo.findById(id), ProductDto.class);
    }

    @Override
    public List<ProductDto> getAllProducts() {
        return ModelMapperConverter.mapAll(productRepo.findAllByOrderById(), ProductDto.class);
    }

    @Override
    public List<ProductDto> getProductsByCategory(CategoryDto cat) {
        return ModelMapperConverter.mapAll(productRepo.findByCategoryOrderByIdAsc(ModelMapperConverter.map(cat, Category.class)), ProductDto.class);
    }

    @Override
    public List<ProductDto> getByCategoryId(Long id) {
        return ModelMapperConverter.mapAll(productRepo.findByCategoryIdOrderByIdAsc(id), ProductDto.class);
    }

    public void saveProfileImage(Product product, MultipartFile productImage) throws IOException {
        if (productImage != null) {

            Path userFolder = Paths.get(System.getProperty("user.home") + "/images/" + product.getName()).toAbsolutePath()
                    .normalize();
            if (!Files.exists(userFolder)) {
                Files.createDirectories(userFolder);
            }
            Files.deleteIfExists(Paths.get(userFolder + product.getName() + "." + "jpg"));
            Files.copy(productImage.getInputStream(), userFolder.resolve(product.getName() + "." + "jpg"),
                    REPLACE_EXISTING);
            product.setImage(setProfileImageUrl(product.getName()));
            productRepo.save(product);

        }
    }

    public String setProfileImageUrl(String productName) {
        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("api/products/images/" + productName + "/" + productName).toUriString();
    }

    @Override
    public ProductDto updateProduct(ProductDto p) {
        return ModelMapperConverter.map(productRepo.save(ModelMapperConverter.map(p, Product.class)), ProductDto.class);
    }

}
