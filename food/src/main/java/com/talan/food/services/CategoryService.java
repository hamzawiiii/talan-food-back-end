package com.talan.food.services;

import java.util.List;


import com.talan.food.dto.CategoryDto;

public interface CategoryService {
	public List<CategoryDto> getAllCategories();
	public CategoryDto getCategorieById(Long id);
	
}


