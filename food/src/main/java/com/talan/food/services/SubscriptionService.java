package com.talan.food.services;

import java.util.List;

import com.talan.food.dto.SubscriptionDto;
import com.talan.food.entities.User;

public interface SubscriptionService {

	SubscriptionDto changeSubscriptionStatus(SubscriptionDto subscriptionDto);

	boolean getSubscriptionStatus(Long productId, Long userId);

	List<SubscriptionDto> findByProductId(Long productId);

	int countNotSeenNotifications(Long userId, boolean seen);

	List<SubscriptionDto> getSubscriptions(Long userId);

	SubscriptionDto changeToSeen(Long id);

	List<User> changeUserNotificationsToVisible(Long productId);

}
