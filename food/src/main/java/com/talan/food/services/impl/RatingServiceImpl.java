package com.talan.food.services.impl;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.talan.food.dto.RatingDto;
import com.talan.food.entities.Product;
import com.talan.food.entities.Rating;
import com.talan.food.entities.User;
import com.talan.food.helpers.ModelMapperConverter;
import com.talan.food.repositories.ProductRepo;
import com.talan.food.repositories.RatingRepo;
import com.talan.food.repositories.UserRepo;
import com.talan.food.services.RatingService;

@Service
public class RatingServiceImpl implements RatingService {
    @Autowired
    RatingRepo ratingRepo;
    @Autowired
    ProductRepo productRepo;
    @Autowired
    UserRepo userRepo;


    @Override
    public double getRating(Long id) {
        double rate = 0;
        Product product = productRepo.findById(id).orElse(null);
            List<Rating> rating = ratingRepo.findByProduct(product);
            for (int i = 0; i < rating.size(); i++) {
                rate = rate + rating.get(i).getValue();
            }
            rate = rate / rating.size();
        return rate;
    }

	@Override
	public RatingDto addRating(RatingDto rating) {

		Optional<User> optionalUser = userRepo.findById(rating.getUser().getId());

		Optional<Product> optionalProduct = productRepo.findById(rating.getProduct().getId());

		List<Rating> list = listRatingProductByuser(rating.getUser().getId(), rating.getProduct().getId());
		if (list.isEmpty()) {

			rating.setUser(optionalUser.isPresent() ? optionalUser.get() : null);
			rating.setProduct(optionalProduct.isPresent() ? optionalProduct.get() : null);
			  return ModelMapperConverter.map(ratingRepo.save(ModelMapperConverter.map(rating, Rating.class)), RatingDto.class);
		} else if (list.size() == 1) 
			list.forEach(el -> el.setValue(rating.getValue()));
		
		
		  return ModelMapperConverter.map(ratingRepo.save(ModelMapperConverter.map(list.get(0), Rating.class)), RatingDto.class);
	}


	@Override
	public List<Rating> listRatingProductByuser(Long idUser, Long idProduct) {
		List<Rating> listRating = ratingRepo.findByProductId(idProduct);
		return listRating.stream().filter(rat -> rat.getUser().getId().equals(idUser)  ).collect(Collectors.toList());
	}



}
