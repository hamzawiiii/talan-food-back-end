package com.talan.food.services.impl;

import com.talan.food.dto.CountProduct;
import com.talan.food.dto.OrderDto;
import com.talan.food.entities.Order;
import com.talan.food.entities.Product;
import com.talan.food.entities.Reservation;
import com.talan.food.helpers.ModelMapperConverter;
import com.talan.food.repositories.OrderRepo;
import com.talan.food.repositories.ProductRepo;
import com.talan.food.repositories.ReservationRepo;
import com.talan.food.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private ProductRepo productRepo;

    @Autowired
    private ReservationRepo reservationRepo;

    // get all orders
    @Override
    public List<OrderDto> getAllOrders() {
        return ModelMapperConverter.mapAll(orderRepo.findAll(), OrderDto.class);
    }

    // find order by id
    @Override
    public OrderDto findOrderById(Long id) {
        return ModelMapperConverter.map(orderRepo.findById(id), OrderDto.class);
    }

    // add/update order
    @Override
    public OrderDto addOrder(OrderDto order) {

        /******evoie par id*****/
        Optional<Product> productOptional = productRepo.findById(order.getProduct().getId());
        order.setProduct(productOptional.isPresent() ? productOptional.get() : null);
        /******fin evoie par id*****/


        /******evoie par id*****/
        Optional<Reservation> reservationOptional = reservationRepo.findById(order.getReservation().getId());
        order.setReservation(reservationOptional.isPresent() ? reservationOptional.get() : null);
        /******fin evoie par id*****/


        Optional<Product> p = productRepo.findById(order.getProduct().getId());
        order.setProduct(p.isPresent() ? p.get() : null);
        Optional<Reservation> r = reservationRepo.findById(order.getReservation().getId());
        order.setReservation(r.isPresent() ? r.get() : null);
        return ModelMapperConverter.map(orderRepo.save(ModelMapperConverter.map(order, Order.class)), OrderDto.class);
    }

    //delete order
    @Override
    public void deleteOrder(Long id) {
        orderRepo.deleteById(id);
    }

    @Override
    public List<OrderDto> getByResrvationId(Long id) {
        return ModelMapperConverter.mapAll(orderRepo.findOrderByReservationId(id), OrderDto.class);

    }

	@Override
	public List<Order> getAllOrdersByUser(Long id) {
		List<Order> listOrder = orderRepo.findAll();

		return listOrder.stream().filter(order -> order.getReservation().getUser().getId() .equals(id))
	.collect(Collectors.toList());
	}



	@Override
	public List<CountProduct> getQuantityByProduct() {
		
		return orderRepo.getQuantityProductSales();
	}

}
