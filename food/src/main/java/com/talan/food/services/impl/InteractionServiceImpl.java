package com.talan.food.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.talan.food.dto.InteractionDto;
import com.talan.food.entities.Interaction;
import com.talan.food.entities.User;
import com.talan.food.helpers.ModelMapperConverter;
import com.talan.food.repositories.InteractionRepo;
import com.talan.food.repositories.UserRepo;
import com.talan.food.services.InteractionService;

@Service
public class InteractionServiceImpl implements InteractionService {

    @Autowired
    private InteractionRepo interactionRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    public List<InteractionDto> getAllInteraction() {
        return ModelMapperConverter.mapAll(interactionRepo.findAllByOrderById(), InteractionDto.class);
    }

    @Override
    public InteractionDto getInteractionById(Long id) {
        return ModelMapperConverter.map(interactionRepo.findById(id), InteractionDto.class);
    }

    @Override
    public InteractionDto addInteraction(InteractionDto interactionDto) {

        /******evoie par id*****/
        Optional<User> userOptional = userRepo.findById(interactionDto.getUser().getId());
        interactionDto.setUser(userOptional.isPresent() ? userOptional.get() : null);
        /******fin evoie par id*****/

        Interaction interaction = ModelMapperConverter.map(interactionDto, Interaction.class);
        interactionRepo.save(interaction);
        //Debut traitement envoie Mail
        SimpleMailMessage sendInteraction = new SimpleMailMessage();
        sendInteraction.setTo("hamza.bouachir@talan.com");
        sendInteraction.setText("Bonjour Gourmet ! Le collaborateur " + " " + interaction.getUser().getFirstName() + " " + interaction.getUser().getLastName() + " " + "a posté une nouvelle" + " " + interaction.getType() + " " + "sous le numéro" + " " + interaction.getId());
        sendInteraction.setSubject("Nouvelle : " + " " + interaction.getType() + " " + "de la part du collaborateur" + " " + interaction.getUser().getFirstName() + " " + interaction.getUser().getLastName());
        javaMailSender.send(sendInteraction);
        //Fin traitement envoie Mail

        return ModelMapperConverter.map(interaction, InteractionDto.class);
    }

    @Override
    public void deleteInteractionById(Long id) {
        interactionRepo.deleteById(id);

    }

    @Override
    public void answerReclamationsClient(Long interactionId, String message) {
        SimpleMailMessage clientResponse = new SimpleMailMessage();
        Interaction interaction = interactionRepo.getById(interactionId);
        clientResponse.setTo(interaction.getUser().getEmail());
        clientResponse.setSubject("Reponse suite à votre réclamation N° " + " " + interaction.getId());
        clientResponse.setText(message);
        javaMailSender.send(clientResponse);


    }


    public void answerToSuggession(Long interactionId) {
        Interaction interaction = interactionRepo.getById(interactionId);
        SimpleMailMessage suugessionResponse = new SimpleMailMessage();
        suugessionResponse.setTo(interaction.getUser().getEmail());
        suugessionResponse.setSubject("Acceptation de votre suggestion N° " + "  " + interaction.getId());
        suugessionResponse.setText("Felicitaions, Votre suggestion du produit" + " " + interaction.getDescription() + " " + "est approuvée !");
        javaMailSender.send(suugessionResponse);

    }


    @Override
    public List<InteractionDto> getInteractionByReclamation() {
        String reclamation = "Reclamation";
        return ModelMapperConverter.mapAll(interactionRepo.findAllByTypeOrderByIdDesc(reclamation), InteractionDto.class);
    }

    @Override
    public List<InteractionDto> getInteractionBySuggestion() {
        String reclamation = "Suggestion";
        return ModelMapperConverter.mapAll(interactionRepo.findAllByTypeOrderByIdDesc(reclamation), InteractionDto.class);
    }


    @Override
    public List<InteractionDto> getInteractionByNote() {
        String note = "Note";
        return ModelMapperConverter.mapAll(interactionRepo.findAllByTypeOrderByIdDesc(note), InteractionDto.class);
    }


}
