package com.talan.food.services;




import java.util.List;

import com.talan.food.dto.RatingDto;
import com.talan.food.entities.Rating;




public interface RatingService {
	public double getRating (Long id);
	public List<Rating> listRatingProductByuser (Long idUser,Long idProduct);
	RatingDto addRating(RatingDto rating);


}
