package com.talan.food.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.talan.food.dto.CategoryDto;
import com.talan.food.helpers.ModelMapperConverter;
import com.talan.food.repositories.CategoryRepo;
import com.talan.food.services.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryRepo categoryRepo;

    @Override
    public List<CategoryDto> getAllCategories() {

        return ModelMapperConverter.mapAll(categoryRepo.findAll(), CategoryDto.class);
    }

    @Override
    public CategoryDto getCategorieById(Long id) {
        return ModelMapperConverter.map(categoryRepo.findById(id), CategoryDto.class);
    }


}
