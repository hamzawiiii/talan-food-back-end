package com.talan.food.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;




@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuDay  {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	//@Temporal(TemporalType.DATE)
	private LocalDate date;
	@ManyToMany (fetch = FetchType.EAGER  /** , cascade = { CascadeType.ALL } */)
	@JoinTable(name = "menu_products", joinColumns = { @JoinColumn(name = "menu_id") }, inverseJoinColumns = {@JoinColumn(name = "product_id") })
	private List<Product> listProducts = new ArrayList<>();
	
	public MenuDay(LocalDate date, List<Product> listProducts) {
		super();
		this.date = date;
		this.listProducts = listProducts;
	}

}
