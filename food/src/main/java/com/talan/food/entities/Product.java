package com.talan.food.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String description;
	private int quantity;
	private double price;
	private String image;
	private boolean displayed;
	@ManyToOne
	@JoinColumn(name = "categoryId")
	private Category category;
	@JsonIgnore
	@ManyToMany (fetch = FetchType.LAZY , mappedBy = "listProducts"  , cascade = { CascadeType.ALL }) 
	private List<MenuDay> listMenus =new ArrayList<>() ;

	public Product(String name, String description, int quantity, double price, String image, boolean displayed,
			Category category) {
		super();
		this.name = name;
		this.description = description;
		this.quantity = quantity;
		this.price = price;
		this.image = image;
		this.displayed = displayed;
		this.category = category;
	}

	/**
	public List<MenuDay> getListMenus() {
		return listMenus;
	}


	public void setListMenus(List<MenuDay> listMenus) {
		this.listMenus = listMenus;
	}
*/


}
