package com.talan.food.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Interaction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String type;
	private String description;
	private int value;
	@ManyToOne @JoinColumn(name="userId" )
	private User user;

	public Interaction(String type, String description, int value, User user) {
		super();
		this.type = type;
		this.description = description;
		this.value = value;
		this.user = user;
	}

}
