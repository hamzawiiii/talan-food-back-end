package com.talan.food.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String firstName;
	private String lastName;
	@Column(unique=true)
	private String email;
	private String password;
	private String phone;
	@ManyToOne(cascade=CascadeType.MERGE)
	@JoinColumn(name = "roleId")
	private Role role;
	@Column(nullable=false, columnDefinition = "BOOLEAN DEFAULT FALSE")
	private boolean notificationsHidden;

	public User(String firstName, String lastName, String email, String password, String phone, Role role) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.role = role;
	}

}
