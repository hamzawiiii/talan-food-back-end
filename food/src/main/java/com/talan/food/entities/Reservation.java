package com.talan.food.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
public class Reservation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private double price;
	@ManyToOne @JoinColumn(name="userId" )
	private User  user;
	private LocalDate date;
	private boolean confirmed;
	@Column(nullable=false, columnDefinition = "BOOLEAN DEFAULT FALSE")
	private boolean isSeen;
	
	

	public Reservation() {
		this.confirmed=false;
	}
	
	public Reservation(double price ,User user, LocalDate date) {
		this.price = price;
		this.user = user;
		this.date = date;
		this.confirmed=false;
	}

}
