package com.talan.food.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.talan.food.dto.SubscriptionDto;
import com.talan.food.services.SubscriptionService;

@RestController
@RequestMapping("/api/subscriptions")
public class SubscriptionController {

	@Autowired
	private SubscriptionService subscriptionService;


	@PostMapping()
	public SubscriptionDto changeSubscriptionStatus(@RequestBody SubscriptionDto subscriptionDto) {
		return subscriptionService.changeSubscriptionStatus(subscriptionDto);

	}


	@GetMapping("/{productId}/{userId}")
	public boolean getSubscriptionStatus(@PathVariable Long productId, @PathVariable Long userId) {
		return subscriptionService.getSubscriptionStatus(productId, userId);

	}


	@GetMapping("/notSeen/{id}")
	public int countNotSeenNotifications(@PathVariable Long id) {
		return subscriptionService.countNotSeenNotifications(id, false);

	}


	@GetMapping("/user/{userId}")

	public List<SubscriptionDto> getSubscriptions(@PathVariable Long userId) {
		return subscriptionService.getSubscriptions(userId);

	}

	//@PreAuthorize("hasAnyAuthority('USER')")
	@PatchMapping("/seen/{id}")
	public SubscriptionDto changeToSeen(@PathVariable("id") Long id) {
		return subscriptionService.changeToSeen(id);

	}

}
