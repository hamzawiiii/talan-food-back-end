package com.talan.food.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.talan.food.dto.Count;
import com.talan.food.dto.MonthIncomes;
import com.talan.food.dto.MonthReservation;
import com.talan.food.dto.Price;
import com.talan.food.dto.ReservationDto;
import com.talan.food.dto.UserIncome;
import com.talan.food.dto.YearIncomes;
import com.talan.food.dto.YearReservation;
import com.talan.food.services.ReservationService;

@RestController
@RequestMapping("/api/reservations")
public class ReservationController {

    @Autowired
    private ReservationService reservationService;


    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping()
    public List<ReservationDto> getAllReservation() {
        return reservationService.getAllReservation();

    }

    @PreAuthorize("hasAuthority('ADMIN')")

  @GetMapping("date") 

    public List<ReservationDto> getAllReservationsByDate(
            @RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {

        return reservationService.getReservationByDate(date);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    @GetMapping("/GetAllById/{id}")
    public ReservationDto getAllReservationById(@PathVariable("id") Long id) {
        return reservationService.getReservationById(id);

    }

   
    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/user/{iduser}/{page}")
    public Page<ReservationDto> getAllReservationByUserId(@PathVariable(name = "iduser") Long iduser ,@PathVariable(name = "page") int page) {
        return reservationService.getReservationByUserId(iduser, page);

    }

    @PreAuthorize("hasAuthority('USER')")
    @PostMapping()
    public ReservationDto addReservation(@RequestBody ReservationDto reservationDto) {
        return reservationService.addReservation(reservationDto);

    }

    @PreAuthorize("hasAuthority('USER')")
    @PostMapping("/Edit")
    public ReservationDto editReservation(@RequestBody ReservationDto reservationDto) {
        return reservationService.editReservation(reservationDto);

    }

    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
    @DeleteMapping("/delete/{id}")
    public void deleteReservation(@PathVariable("id") Long id) {
        reservationService.deleteReservationById(id);
    }

    @PreAuthorize("hasAuthority('USER')")
    @PostMapping("confirm")
    public boolean confirmReservation(@RequestBody ReservationDto reservationDto) {
        ReservationDto reservation = reservationService.confirmReservation(reservationDto);
        return reservation != null;
    }

    @PreAuthorize("hasAuthority('USER')")
    @DeleteMapping("annulation/{reservationId}")
    public boolean annulReservation(@PathVariable("reservationId") Long reservationId) {

        return reservationService.annulerReservation(reservationId);
    }

    @GetMapping("/static")
    public List<Count> getAllReservationByDate()   {
        return reservationService.getcountReservation();

    }
    
    @GetMapping("/static/month")
    public List<MonthReservation> getAllReservationByMonth()   {
        return reservationService.getReservationByMonth();

    }
    
    @GetMapping("/static/year")
    public List<YearReservation> getAllReservationByYear()   {
        return reservationService.getReservationByYear();

    }
    @GetMapping("/incomes")
    public List<Price> getAllPIncomesReservationStatic()   {
        return reservationService.getIncomesByDate();

    }
    
    @GetMapping("/incomes/user")
    public List<UserIncome> getAllPIncomesReservationByUser()   {
        return reservationService.getIncomesByUser();

    }
    
    @GetMapping("/incomes/month")
    public List<MonthIncomes> getAllPIncomesReservationByMonth()   {
        return reservationService.getIncomesByMonth();

    }
    
    @GetMapping("/incomes/year")
    public List<YearIncomes> getAllPIncomesReservationByYear()   {
        return reservationService.getIncomesByYear();

    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/countNotSeen")
    public int countByIsSeen() {
        return reservationService.countByIsSeen(false);

    }
    
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PatchMapping("/seen/{id}")
    public ReservationDto changeToSeen(@PathVariable("id") Long id) {
        return reservationService.changeToSeen(id);

    }


}
