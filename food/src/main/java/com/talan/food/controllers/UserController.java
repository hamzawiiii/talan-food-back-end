package com.talan.food.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.talan.food.dto.UserDto;
import com.talan.food.services.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    //    Sign Up
    @SuppressWarnings("rawtypes")
	@PostMapping("signup")
    public ResponseEntity signup(@RequestBody UserDto userDto) {
        UserDto user = userService.signup(userDto);
        if (user == null)
            return new ResponseEntity<>("Cette adresse e-mail est déjà utilisée", HttpStatus.NOT_ACCEPTABLE);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    //    Get user by Id
    @GetMapping("{userId}")
    public ResponseEntity<UserDto> getUserById(@PathVariable Long userId) {
        if (userService.getUserById(userId) == null)
            return new ResponseEntity<>(userService.getUserById(userId), HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(userService.getUserById(userId), HttpStatus.OK);
    }
    
    @PatchMapping("{userId}")
    public ResponseEntity<UserDto> changeNotificationsVisibility(@PathVariable Long userId,@RequestBody boolean hidden) {
       
        return new ResponseEntity<>(userService.changeNotificationsVisibility(hidden,userId), HttpStatus.OK);
    }


}
