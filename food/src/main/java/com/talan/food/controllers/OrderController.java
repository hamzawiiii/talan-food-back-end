package com.talan.food.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.talan.food.dto.CountProduct;
import com.talan.food.dto.OrderDto;
import com.talan.food.entities.Order;
import com.talan.food.services.OrderService;


@RestController
@RequestMapping("/api/ordres")
@CrossOrigin
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    @GetMapping
    public List<OrderDto> getAllOrders() {
        return orderService.getAllOrders();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    @GetMapping("/{id}")
    public OrderDto findOrderById(@PathVariable Long id) {
        return orderService.findOrderById(id);
    }

    @PreAuthorize("hasAuthority('USER')")
    @PostMapping
    public OrderDto addOrder(@RequestBody OrderDto order) {
        return orderService.addOrder(order);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    @DeleteMapping("/{id}")
    public void deleteOrder(@PathVariable Long id) {
        orderService.deleteOrder(id);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    @GetMapping("/reservation/{id}")
    public List<OrderDto> findOrderByReservation(@PathVariable Long id) {
        return orderService.getByResrvationId(id);
    }
	@GetMapping("/user/{id}")
	public ResponseEntity<List<Order>> getAllOrdersByUser(@PathVariable Long id) {

		return ResponseEntity.ok().body(orderService.getAllOrdersByUser(id));

	}


	@GetMapping("/product/quantity")
	public List<CountProduct> getQuantityProduct() {
		return orderService.getQuantityByProduct();
	}

}
