package com.talan.food.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.talan.food.dto.RatingDto;
import com.talan.food.entities.Rating;
import com.talan.food.services.RatingService;

@RestController
@RequestMapping("/api/products/ratings")
public class RatingController {
    @Autowired
    RatingService ratingService;

    @GetMapping("/{id}")
    public double getRating(@PathVariable Long id) {
        return ratingService.getRating(id);
    }

    @PreAuthorize("hasAuthority('USER')")
    @PostMapping
    public RatingDto addRating(@RequestBody RatingDto rating) {

        return ratingService.addRating(rating);

    }
    
    @GetMapping("/{idUser}/{idProduct}")
    public List<Rating> ratingProductByUser(@PathVariable Long idUser, @PathVariable Long idProduct) {

        return ratingService.listRatingProductByuser(idUser, idProduct);

    }

}
