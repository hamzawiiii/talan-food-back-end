package com.talan.food.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.talan.food.dto.InteractionDto;
import com.talan.food.services.InteractionService;

@RestController
@CrossOrigin
@RequestMapping("/api/interactions")
public class InteractionController {

    @Autowired
    InteractionService interactionService;


    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/All")
    public List<InteractionDto> getAllInteractions() {
        return interactionService.getAllInteraction();
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("reclamations")
    public List<InteractionDto> getAllReclamations() {

        return interactionService.getInteractionByReclamation();
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("suggestions")
    public List<InteractionDto> getAllSuggestions() {

        return interactionService.getInteractionBySuggestion();
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("notes")
    public List<InteractionDto> getAllNote() {

        return interactionService.getInteractionByNote();
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/GetById/{id}")
    public InteractionDto getInteractionsById(@PathVariable("id") Long id) {

        return interactionService.getInteractionById(id);
    }


    @PreAuthorize("hasAuthority('USER')")
    @PostMapping()
    public InteractionDto addInteraction(@RequestBody InteractionDto interactionDto) {

        return interactionService.addInteraction(interactionDto);

    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("{id}")
    public void deleteInteraction(@PathVariable("id") Long id) {

        interactionService.deleteInteractionById(id);
    }


    @GetMapping("/reclamation/response/{idRec}")
    public void clientAnswer(@PathVariable("idRec") Long id, @RequestParam("message") String message) {
        interactionService.answerReclamationsClient(id, message);

    }


    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("suggession/response/{idRec}")
    public void responseSuggession(@PathVariable("idRec") Long id) {
        interactionService.answerToSuggession(id);

    }


}
