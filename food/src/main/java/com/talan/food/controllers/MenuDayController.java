package com.talan.food.controllers;

import com.talan.food.dto.MenuDayDto;
import com.talan.food.services.MenuDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/menus")
@CrossOrigin
public class MenuDayController {

    @Autowired
    MenuDayService menuDayService;

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("menu")
    @Transactional
    public MenuDayDto addMenu(@RequestBody MenuDayDto menu) {
        /**
         LocalDate menuDate = menu.getDate();
         List<MenuDayDto> listMenus = new ArrayList<MenuDayDto>();
         listMenus = menuDayService.getAllMenus();
         try {
         if (listMenus.size()!=0) {

         for(int i = 0 ; i < listMenus.size() ; i++) {
         if (menuDate.equals(listMenus.get(i).getDate()) ) {
         menuDayService.deleteMenuDayById(listMenus.get(i).getId());
         }
         }
         }
         } catch (Exception e) {
         e.fillInStackTrace();
         } finally {
         return menuDayService.saveMenuDay(menu);
         }
         */
        return menuDayService.saveMenuDay(menu);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/product/{id}/{prodId}")
    public MenuDayDto addProductToMenu(@PathVariable("id") Long menuId, @PathVariable("prodId") Long prodId) {
        return menuDayService.addProductToMenuDay(menuId, prodId);
    }



    @GetMapping
    public List<MenuDayDto> getAllMenus() {
        return menuDayService.getAllMenus();
    }

    @GetMapping("/{id}")
    public MenuDayDto getMenuById(@PathVariable("id") Long id) {
        return menuDayService.getMenuDayById(id);
    }

    @GetMapping("/menu/date")
    public MenuDayDto getMenuByDate(@RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        return menuDayService.getMenuDayByDate(date);
    }

    //@PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/{id}")
    public void deleteMenu(@PathVariable("id") Long id) {
        menuDayService.deleteMenuDayById(id);
    }
}

