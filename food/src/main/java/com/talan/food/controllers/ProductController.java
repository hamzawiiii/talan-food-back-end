package com.talan.food.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.talan.food.dto.CategoryDto;
import com.talan.food.dto.ProductDto;
import com.talan.food.entities.Category;
import com.talan.food.entities.Product;
import com.talan.food.helpers.ModelMapperConverter;
import com.talan.food.services.CategoryService;
import com.talan.food.services.ProductService;

@RestController
@RequestMapping("/api/products")
@CrossOrigin
public class ProductController {

    @Autowired
    ProductService productService;
    @Autowired
    CategoryService categoryService;

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/new/product")
    public ProductDto addProduct(@RequestParam(value = "name") String name,
                                 @RequestParam(value = "description") String description, @RequestParam(value = "price") double price,
                                 @RequestParam(value = "quantity") int quantity, @RequestParam(value = "displayed") boolean displayed,
                                 @RequestParam(value = "category") Long categoryId,
                                 @RequestParam(value = "profileImage") MultipartFile profileImage) throws IOException {
        Category category = ModelMapperConverter.map(categoryService.getCategorieById(categoryId), Category.class);
        ProductDto prod = new ProductDto(name, description, quantity, price, null, displayed, category);
        return productService.saveProduct(prod, profileImage);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/product")
    public ProductDto updateProduct(@RequestParam(value = "id") Long id, @RequestParam(value = "name") String name,
                                    @RequestParam(value = "description") String description, @RequestParam(value = "price") double price,
                                    @RequestParam(value = "quantity") int quantity, @RequestParam(value = "displayed") boolean displayed,
                                    @RequestParam(value = "category") Long categoryId,
                                    @RequestParam(value = "profileImage", required = false) MultipartFile profileImage) throws IOException {
        Category category = ModelMapperConverter.map(categoryService.getCategorieById(categoryId), Category.class);
        ProductDto prod = new ProductDto(id, name, description, quantity, price, null, displayed, category);

        return productService.saveProduct(prod, profileImage);

    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable("id") Long id) {
        productService.deleteProductById(id);
    }

    @GetMapping("/product/{id}")
    public ProductDto getProductById(@PathVariable("id") Long id) {
        return productService.getProductById(id);
    }

    @GetMapping
    public List<ProductDto> getAllProducts() {
        return productService.getAllProducts();
    }

    @GetMapping("/category")
    public List<ProductDto> getProductsByCategory(@RequestBody CategoryDto cat) {
        return productService.getProductsByCategory(cat);
    }

    @GetMapping("/catid/{catId}")
    public List<ProductDto> getProductsByCategoryId(@PathVariable("catId") Long catId) {
        return productService.getByCategoryId(catId);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/image")
    public void updateProfileImage(@RequestParam(value = "product") Long id,
                                   @RequestParam(value = "profileImage") MultipartFile profileImage) throws IOException {
        ProductDto products = productService.getProductById(id);
        productService.saveProfileImage(ModelMapperConverter.map(products, Product.class), profileImage);
    }

    @GetMapping(path = "/images/{productName}/{fileName}", produces = "image/jpeg")
    public byte[] getProfileImage(@PathVariable("productName") String productName,
                                  @PathVariable("fileName") String fileName) throws IOException {
        return Files.readAllBytes(
                Paths.get(System.getProperty("user.home") + "/images/" + productName + "/" + fileName + ".jpg"));

    }

    @PreAuthorize("hasAuthority('USER')")
    @PostMapping("/quantity/{id}/{q}")
    public ProductDto updateProductQuantity(@PathVariable("id") Long id, @PathVariable("q") int q) {
        ProductDto prod = productService.getProductById(id);
        prod.setQuantity(q);
        return productService.updateProduct(prod);
    }

}
