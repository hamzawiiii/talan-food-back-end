package com.talan.food.dto;

import com.talan.food.entities.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuDayDto {

	private Long id;
	private LocalDate date;
	private List<Product> listProducts;

	public MenuDayDto(LocalDate date, List<Product> listProducts) {
		super();
		this.date = date;
		this.listProducts = listProducts;
	}

}
