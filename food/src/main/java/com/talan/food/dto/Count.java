package com.talan.food.dto;

import java.time.LocalDate;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Data
@Getter
@Setter
@NoArgsConstructor

public class Count {
	private LocalDate date;
	private Long count;

	 public Count(LocalDate date,Long count){
		   
		   this.date = date;
		   this.count = count;
		  
		 }
}
