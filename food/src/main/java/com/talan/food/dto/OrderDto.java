package com.talan.food.dto;

import com.talan.food.entities.Product;
import com.talan.food.entities.Reservation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {

	private Long id;	
	private  Product product;
	private int quantity;
	private  Reservation reservation;

	public OrderDto(Product product, int quantity, Reservation reservation) {
		super();
		this.product = product;
		this.quantity = quantity;
		this.reservation = reservation;
	}

}
