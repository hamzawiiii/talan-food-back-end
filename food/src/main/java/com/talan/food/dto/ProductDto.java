package com.talan.food.dto;

import com.talan.food.entities.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {

	private Long id;
	private String name;
	private String description;
	private int quantity;
	private double price;
	private String image;
	private boolean displayed;
	private Category category;

	public ProductDto(String name, String description, int quantity, double price, String image, boolean displayed,
			Category category) {
		super();
		this.name = name;
		this.description = description;
		this.quantity = quantity;
		this.price = price;
		this.image = image;
		this.displayed = displayed;
		this.category = category;
	}

}
