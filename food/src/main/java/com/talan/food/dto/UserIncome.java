package com.talan.food.dto;



import com.talan.food.entities.User;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Data
@Getter
@Setter
@NoArgsConstructor

public class UserIncome {
	
	private User  user ;
	private double price;

	 public UserIncome(User user,double price){
		   
		   this. user =  user;
		   this.price = price;
		  
		 }
}
