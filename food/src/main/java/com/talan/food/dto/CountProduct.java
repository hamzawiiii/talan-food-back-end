package com.talan.food.dto;

import com.talan.food.entities.Product;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Data
@NoArgsConstructor
@Getter
@Setter

public class CountProduct {
	private  Product product;
	private  Long quantity;
	public CountProduct(Product product, Long  quantity) {
		
		this.product = product;
		this.quantity = quantity;
		
	}
}
