package com.talan.food.dto;

import java.time.LocalDate;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class  CountReservation{

	private LocalDate date;
	private int count;

	 public CountReservation(int count,LocalDate date){
		   
		   this.date = date;
		   this.count = count;
		  
		 }
}
