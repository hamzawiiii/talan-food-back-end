package com.talan.food.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
public class YearReservation {
	private int date ;
	private Long count;

	 public YearReservation (int date,Long count){
		   
		   this. date=  date;
		   this.count = count;
		  
		 }
}
