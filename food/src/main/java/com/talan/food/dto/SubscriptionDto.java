package com.talan.food.dto;

import java.time.LocalDate;

import com.talan.food.entities.Product;
import com.talan.food.entities.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class SubscriptionDto {

	private Long id;

	private User user;

	private Product product;
	
	private boolean seen;
	
	private LocalDate menuDate;
	
	private boolean activated;

}
