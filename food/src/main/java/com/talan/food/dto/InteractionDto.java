package com.talan.food.dto;
import com.talan.food.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InteractionDto {
	

	private Long id;
	private String type;
	private String description;
	private int value;
	private User user;
	int number;

	public InteractionDto(String type, String description, int value, User user) {
		this.type = type;
		this.description = description;
		this.value = value;
		this.user = user;
	}

}
