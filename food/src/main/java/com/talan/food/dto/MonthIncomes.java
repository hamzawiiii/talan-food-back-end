package com.talan.food.dto;




import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Data
@Getter
@Setter
@NoArgsConstructor
public class MonthIncomes {
	private int date ;
	private double price;

	 public MonthIncomes (int date,double price){
		   
		   this. date=  date;
		   this.price = price;
		  
		 }
}
