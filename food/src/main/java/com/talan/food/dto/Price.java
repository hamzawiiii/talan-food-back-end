package com.talan.food.dto;
import java.time.LocalDate;


import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Data
@Getter
@Setter
@NoArgsConstructor
public class Price {
	
		private LocalDate date;
		private double price;

		 public Price(LocalDate date,double price){
			   
			   this.date = date;
			   this.price = price;
			  
			 }
	
}
