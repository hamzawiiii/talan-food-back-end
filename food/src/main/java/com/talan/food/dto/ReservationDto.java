package com.talan.food.dto;

import com.talan.food.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReservationDto {

	private Long id;
	private double price;
	private User user;
	private LocalDate date;
	private boolean confirmed;
	private boolean isSeen;

	public ReservationDto(double price, User user, LocalDate date) {
		this.price = price;
		this.user = user;
		this.date = date;
		this.confirmed = false;

	}

}
