package com.talan.food.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
public class MonthReservation {
	private int date ;
	private Long count;

	 public MonthReservation (int date,Long count){
		   
		   this. date=  date;
		   this.count = count;
		  
		 }
}
