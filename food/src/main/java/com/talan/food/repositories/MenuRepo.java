package com.talan.food.repositories;

import com.talan.food.entities.MenuDay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface MenuRepo extends  JpaRepository<MenuDay,Long> {

	MenuDay findByDate(LocalDate date);
	
	List<MenuDay> findAllByOrderByDate();
}
