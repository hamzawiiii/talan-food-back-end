package com.talan.food.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.talan.food.dto.Count;
import com.talan.food.dto.MonthIncomes;
import com.talan.food.dto.MonthReservation;
import com.talan.food.dto.Price;
import com.talan.food.dto.UserIncome;
import com.talan.food.dto.YearIncomes;
import com.talan.food.dto.YearReservation;
import com.talan.food.entities.Reservation;


@Repository
public interface ReservationRepo extends JpaRepository<Reservation, Long> {


	Page<Reservation> findByUserIdOrderByIdDesc(Long id, Pageable p);

	List<Reservation> findAllByOrderById();

	List<Reservation> findAllByDateOrderById(LocalDate date);

	List<Reservation> findByUserId(Long id);

	long count();

	int countByDate(LocalDate date);

	List<Reservation> findAllByOrderByDateAsc();

	@Query(value = "select new com.talan.food.dto.Count( date,count(p) )from Reservation p group by  p.date  ORDER BY p.date")

	public List<Count> getcountReservationByDate();

	@Query(value = "select new com.talan.food.dto.MonthReservation(EXTRACT(month from p.date),count(p)) from Reservation p WHERE p.date >= '2023-01-01' group by EXTRACT(month from p.date) ORDER BY EXTRACT(month from p.date)")

	public List<MonthReservation> getReservationByMonth();


	@Query(value = "select new com.talan.food.dto.YearReservation(EXTRACT(Year from p.date),count(p)) from Reservation p group by EXTRACT(Year from p.date) ORDER BY EXTRACT(Year from p.date)")

	public List<YearReservation> getReservationByYear();

	@Query(value = "select new com.talan.food.dto.Price( date,SUM(p.price)) from Reservation p group by  p.date ORDER BY p.date ASC")

	public List<Price> getPriceReservationByDate();

	@Query(value = "select new com.talan.food.dto.UserIncome (  user,SUM(p.price)) from Reservation p group by  p. user ORDER BY SUM(p.price) DESC ")

	public List<UserIncome> getPriceReservationByUser();

	@Query(value = "select new com.talan.food.dto.MonthIncomes(EXTRACT(month from p.date),SUM(p.price)) from Reservation p WHERE p.date >= '2023-01-01' group by EXTRACT(month from p.date) ORDER BY EXTRACT(month from p.date)")

	public List<MonthIncomes> getIncomeByMonth();

	@Query(value = "select new com.talan.food.dto.YearIncomes(EXTRACT(YEAR from p.date),SUM(p.price)) from Reservation p group by EXTRACT(YEAR from p.date) ORDER BY EXTRACT(YEAR from p.date)")

	public List<YearIncomes> getIncomeByYear();

	List<Reservation> findAllByOrderByIdDesc();

	int countByIsSeen(boolean isSeen);




}
