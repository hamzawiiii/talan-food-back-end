package com.talan.food.repositories;


import com.talan.food.entities.Category;
import com.talan.food.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepo extends  JpaRepository<Product, Long> {

	List<Product> findAllByOrderById();
	List<Product> findByCategoryOrderByIdAsc(Category cat);
	List<Product> findByCategoryIdOrderByIdAsc(Long id);
	
}
