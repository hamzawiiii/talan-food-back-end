package com.talan.food.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.talan.food.dto.CountProduct;
import com.talan.food.entities.Order;

public interface OrderRepo extends JpaRepository<Order, Long> {

	public List<Order> findOrderByReservationId (Long id);
	
	@Query(value = "select new com.talan.food.dto.CountProduct( product,SUM(p.quantity)   )from Order p group by  p.product")

	public List<CountProduct> getQuantityProductSales();
}
