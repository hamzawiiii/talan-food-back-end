package com.talan.food.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.talan.food.entities.Product;
import com.talan.food.entities.Rating;

@Repository
public interface RatingRepo extends JpaRepository<Rating, Long> {
	public List<Rating> findByProduct(Product product); 
	public List<Rating> findByUserId(Long idUser);
	public List<Rating> findByProductId(Long idProduct); 
}
