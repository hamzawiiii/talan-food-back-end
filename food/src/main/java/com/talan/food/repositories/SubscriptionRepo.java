package com.talan.food.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.talan.food.entities.Subscription;

@Repository
public interface SubscriptionRepo extends JpaRepository<Subscription, Long> {
	
	List<Subscription> findByProductIdAndUserId(Long productId,Long userId);
	List<Subscription> findByProductIdAndActivated(Long productId,boolean activated);
	int countByMenuDateNotNullAndUserIdAndSeen(Long userId,boolean seen);
	Subscription findFirst1ByProductIdAndUserId(Long productId,Long userId);
	List<Subscription> findByMenuDateNotNullAndUserIdOrderByIdDesc(Long userId);
}
